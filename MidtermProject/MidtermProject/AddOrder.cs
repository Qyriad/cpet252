﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// REST client includes 

//using System.Net;
//
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json; // be sure to add reference
using Newtonsoft.Json;
using System.Runtime.Serialization; // for [DataContract] attribute
using System.Net;
using System.Web.Script.Serialization;
using System.IO;

namespace MidtermProject
{
    public partial class AddOrder : Form
    {
        Store store;
        Book book;

        string serverIP = Form1.serverIP;
        public AddOrder(Store s, Book b)
        {
            InitializeComponent();
            this.Text = "Add Order";
            store = s;
            book = b;

            storeIDBox.Text = s.stor_name;
            titleIDBox.Text = b.title;
            orderNumBox.Text = generateOrderNum().ToString();
            orderDateBox.Text = DateTime.Now.ToString("yyyyMMdd");
        }

        //The following function generates a random order number
        public int generateOrderNum()
        {
            Random slumpGenerator = new Random();
            int orderNum = slumpGenerator.Next(1000, 99999999);

            return orderNum;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //The following function sends the add request to the server
        //The information for the object is taken from the information in the
        //text boxes of the form
        private void createOrderButton_Click(object sender, EventArgs e)
        {
            string endPoint = "http://" + serverIP + "/sales";

            Sales sale = new Sales();
            sale.stor_id = store.stor_id;
            sale.ord_num = orderNumBox.Text.Trim();
            sale.ord_date = orderDateBox.Text.Trim();
            sale.qty = qtyBox.Text.Trim();
            sale.payterms = payTermsComboBox.Text.Trim();
            sale.title_id = book.title_id;

            var json = new JavaScriptSerializer().Serialize(sale);

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "POST";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (System.Net.HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

            this.Close();
        }
    }
}
