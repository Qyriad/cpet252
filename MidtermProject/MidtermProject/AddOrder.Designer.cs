﻿namespace MidtermProject
{
    partial class AddOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddOrder));
            this.storeIDBox = new System.Windows.Forms.TextBox();
            this.orderNumBox = new System.Windows.Forms.TextBox();
            this.orderDateBox = new System.Windows.Forms.TextBox();
            this.qtyBox = new System.Windows.Forms.TextBox();
            this.titleIDBox = new System.Windows.Forms.TextBox();
            this.createOrderButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.payTermsComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // storeIDBox
            // 
            this.storeIDBox.Location = new System.Drawing.Point(32, 56);
            this.storeIDBox.Name = "storeIDBox";
            this.storeIDBox.ReadOnly = true;
            this.storeIDBox.Size = new System.Drawing.Size(145, 20);
            this.storeIDBox.TabIndex = 0;
            // 
            // orderNumBox
            // 
            this.orderNumBox.Location = new System.Drawing.Point(32, 110);
            this.orderNumBox.Name = "orderNumBox";
            this.orderNumBox.ReadOnly = true;
            this.orderNumBox.Size = new System.Drawing.Size(145, 20);
            this.orderNumBox.TabIndex = 1;
            // 
            // orderDateBox
            // 
            this.orderDateBox.Location = new System.Drawing.Point(32, 167);
            this.orderDateBox.Name = "orderDateBox";
            this.orderDateBox.ReadOnly = true;
            this.orderDateBox.Size = new System.Drawing.Size(145, 20);
            this.orderDateBox.TabIndex = 2;
            // 
            // qtyBox
            // 
            this.qtyBox.Location = new System.Drawing.Point(283, 56);
            this.qtyBox.Name = "qtyBox";
            this.qtyBox.Size = new System.Drawing.Size(145, 20);
            this.qtyBox.TabIndex = 1;
            // 
            // titleIDBox
            // 
            this.titleIDBox.Location = new System.Drawing.Point(283, 167);
            this.titleIDBox.Name = "titleIDBox";
            this.titleIDBox.ReadOnly = true;
            this.titleIDBox.Size = new System.Drawing.Size(145, 20);
            this.titleIDBox.TabIndex = 4;
            // 
            // createOrderButton
            // 
            this.createOrderButton.Location = new System.Drawing.Point(283, 226);
            this.createOrderButton.Name = "createOrderButton";
            this.createOrderButton.Size = new System.Drawing.Size(113, 28);
            this.createOrderButton.TabIndex = 3;
            this.createOrderButton.Text = "Create Order";
            this.createOrderButton.UseVisualStyleBackColor = true;
            this.createOrderButton.Click += new System.EventHandler(this.createOrderButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(402, 226);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(113, 28);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Store ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Order Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Order Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Quantity:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Pay Terms:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(280, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Title ID:";
            // 
            // payTermsComboBox
            // 
            this.payTermsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.payTermsComboBox.FormattingEnabled = true;
            this.payTermsComboBox.Items.AddRange(new object[] {
            "Net 60",
            "Net 30",
            "On Invoice"});
            this.payTermsComboBox.Location = new System.Drawing.Point(283, 110);
            this.payTermsComboBox.Name = "payTermsComboBox";
            this.payTermsComboBox.Size = new System.Drawing.Size(145, 21);
            this.payTermsComboBox.TabIndex = 2;
            // 
            // AddOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 266);
            this.Controls.Add(this.payTermsComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.createOrderButton);
            this.Controls.Add(this.titleIDBox);
            this.Controls.Add(this.qtyBox);
            this.Controls.Add(this.orderDateBox);
            this.Controls.Add(this.orderNumBox);
            this.Controls.Add(this.storeIDBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddOrder";
            this.Text = "AddOrder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox storeIDBox;
        private System.Windows.Forms.TextBox orderNumBox;
        private System.Windows.Forms.TextBox orderDateBox;
        private System.Windows.Forms.TextBox qtyBox;
        private System.Windows.Forms.TextBox titleIDBox;
        private System.Windows.Forms.Button createOrderButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox payTermsComboBox;
    }
}