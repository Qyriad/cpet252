﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.IO;

namespace MidtermProject
{
    public partial class UpdateOrder : Form
    {
        Sales order;

        string serverIP = Form1.serverIP;
        public UpdateOrder(Sales s)
        {
            InitializeComponent();
            this.Text = "Update Order";
            order = s;

            storeIDBox.Text = order.stor_id;
            storeNameBox.Text = order.stor_name;
            orderNumBox.Text = order.ord_num;
            orderDateBox.Text = order.ord_date;
            qtyBox.Text = order.qty;
            payTermsComboBox.Text = order.payterms;
            titleIDBox.Text = order.title_id;
            titleBox.Text = order.title;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //The following function sends the put request to the server
        //The information for the object is taken from the information in the
        //text boxes of the form
        private void updateOrderButton_Click(object sender, EventArgs e)
        {
            Sales sale = new Sales();
            sale.stor_id = order.stor_id;
            sale.ord_num = orderNumBox.Text.Trim();
            sale.ord_date = orderDateBox.Text.Trim();
            sale.qty = qtyBox.Text.Trim();
            sale.payterms = payTermsComboBox.Text.Trim();
            sale.title_id = order.title_id;

            var json = new JavaScriptSerializer().Serialize(sale);

            string endPoint = "http://" + serverIP + "/sales/" + sale.ord_num;

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "PUT";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (System.Net.HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

            this.Close();
        }
    }
}
