﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;

namespace MidtermProject
{
    public partial class AddStore : Form
    {
        string serverIP = Form1.serverIP;
        public AddStore()
        {
            InitializeComponent();
            this.Text = "Add Store";

            storeIDBox.Text = generateStoreID().ToString();
        }

        //The following function generates a random number for the store id
        public int generateStoreID()
        {
            Random slumpGenerator = new Random();
            int storeID = slumpGenerator.Next(1000, 9999);

            return storeID;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //The following function sends the add request to the server
        //The information for the object is taken from the information in the
        //text boxes of the form
        private void createStoreButton_Click(object sender, EventArgs e)
        {
            string endPoint = "http://" + serverIP + "/stores";

            Store store = new Store();
            store.stor_id = storeIDBox.Text.Trim();
            store.stor_name = storeNameBox.Text.Trim();
            store.stor_address = storeAddressBox.Text.Trim();
            store.city = cityBox.Text.Trim();
            store.state = stateComboBox.Text.Trim();
            store.zip = zipBox.Text.Trim();

            var json = new JavaScriptSerializer().Serialize(store);

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "POST";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (System.Net.HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

            this.Close();
        }
    }
}
