﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.IO;

namespace MidtermProject
{
    public partial class UpdateBook : Form
    {
        string serverIP = Form1.serverIP;
        Book book;
        public UpdateBook(Book b)
        {
            InitializeComponent();
            this.Text = "Update Book";

            book = b;

            titleIDBox.Text = book.title_id;
            titleBox.Text = book.title;
            typeBox.Text = book.type;
            pubIDComboBox.Text = book.pub_id;
            priceBox.Text = book.price;
            advanceBox.Text = book.advance;
            royaltyBox.Text = book.royalty;
            ytdBox.Text = book.ytd_sales;
            notesBox.Text = book.notes;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //The following function sends the put request to the server
        //The information for the object is taken from the information in the
        //text boxes of the form
        private void updateBookButton_Click(object sender, EventArgs e)
        {
            Book book = new Book();

            book.title_id = titleIDBox.Text.Trim();
            book.title = titleBox.Text.Trim();
            book.notes = notesBox.Text.Trim();
            book.price = priceBox.Text.Trim();
            book.pub_id = pubIDComboBox.Text.Trim();
            book.royalty = royaltyBox.Text.Trim();
            book.type = typeBox.Text.Trim();
            book.advance = advanceBox.Text.Trim();
            book.ytd_sales = ytdBox.Text.Trim();

            var json = new JavaScriptSerializer().Serialize(book);

            string endPoint = "http://" + serverIP + "/books/" + book.title_id;

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "PUT";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (System.Net.HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

            this.Close();
        }

    }
}
