﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.IO;


namespace MidtermProject
{
    public partial class AddBook : Form
    {
        string serverIP = Form1.serverIP;
        public AddBook()
        {
            InitializeComponent();
            this.Text = "Add Book";

            titleIDBox.Text = titleIDGenerator();
        }

        //The following function generates a random title id for the book
        //The id will be in the for of two random letters followed by four random numbers
        public string titleIDGenerator()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[2];
            var random = new Random();
            string storeID;

            //Select the random letters
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            //Select the random numbers
            Random slumpGenerator = new Random();
            string storeNum = slumpGenerator.Next(1000, 9999).ToString();

            //Combine the random letters with the random numbers to create the store id
            storeID = stringChars[0].ToString() + stringChars[1].ToString() + storeNum;

            return storeID;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //The following function sends the add request to the server
        //The information for the object is taken from the information in the
        //text boxes of the form
        private void createBookButton_Click(object sender, EventArgs e)
        {
            string endPoint = "http://" + serverIP + "/books";

            Book book = new Book();

            book.title_id = titleIDBox.Text.Trim();
            book.title = titleBox.Text.Trim();
            book.notes = notesBox.Text.Trim();
            book.price = priceBox.Text.Trim();
            book.pub_id = pubIDComboBox.Text.Trim();
            book.royalty = royaltyBox.Text.Trim();
            book.type = typeBox.Text.Trim();
            book.advance = advanceBox.Text.Trim();
            book.ytd_sales = ytdBox.Text.Trim();

            var json = new JavaScriptSerializer().Serialize(book);

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "POST";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (System.Net.HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

            this.Close();
        }
    }

}
