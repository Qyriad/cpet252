﻿namespace MidtermProject
{
    partial class UpdateOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateOrder));
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.updateOrderButton = new System.Windows.Forms.Button();
            this.titleIDBox = new System.Windows.Forms.TextBox();
            this.qtyBox = new System.Windows.Forms.TextBox();
            this.orderDateBox = new System.Windows.Forms.TextBox();
            this.orderNumBox = new System.Windows.Forms.TextBox();
            this.storeIDBox = new System.Windows.Forms.TextBox();
            this.payTermsComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.storeNameBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.titleBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(260, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Title ID:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(260, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Pay Terms:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(260, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Quantity:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Order Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Order Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Store ID:";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(389, 258);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(113, 28);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // updateOrderButton
            // 
            this.updateOrderButton.Location = new System.Drawing.Point(270, 258);
            this.updateOrderButton.Name = "updateOrderButton";
            this.updateOrderButton.Size = new System.Drawing.Size(113, 28);
            this.updateOrderButton.TabIndex = 3;
            this.updateOrderButton.Text = "Update Order";
            this.updateOrderButton.UseVisualStyleBackColor = true;
            this.updateOrderButton.Click += new System.EventHandler(this.updateOrderButton_Click);
            // 
            // titleIDBox
            // 
            this.titleIDBox.Location = new System.Drawing.Point(263, 156);
            this.titleIDBox.Name = "titleIDBox";
            this.titleIDBox.ReadOnly = true;
            this.titleIDBox.Size = new System.Drawing.Size(145, 20);
            this.titleIDBox.TabIndex = 18;
            // 
            // qtyBox
            // 
            this.qtyBox.Location = new System.Drawing.Point(263, 45);
            this.qtyBox.Name = "qtyBox";
            this.qtyBox.Size = new System.Drawing.Size(145, 20);
            this.qtyBox.TabIndex = 1;
            // 
            // orderDateBox
            // 
            this.orderDateBox.Location = new System.Drawing.Point(12, 213);
            this.orderDateBox.Name = "orderDateBox";
            this.orderDateBox.ReadOnly = true;
            this.orderDateBox.Size = new System.Drawing.Size(145, 20);
            this.orderDateBox.TabIndex = 16;
            // 
            // orderNumBox
            // 
            this.orderNumBox.Location = new System.Drawing.Point(12, 156);
            this.orderNumBox.Name = "orderNumBox";
            this.orderNumBox.ReadOnly = true;
            this.orderNumBox.Size = new System.Drawing.Size(145, 20);
            this.orderNumBox.TabIndex = 15;
            // 
            // storeIDBox
            // 
            this.storeIDBox.Location = new System.Drawing.Point(12, 45);
            this.storeIDBox.Name = "storeIDBox";
            this.storeIDBox.ReadOnly = true;
            this.storeIDBox.Size = new System.Drawing.Size(145, 20);
            this.storeIDBox.TabIndex = 14;
            // 
            // payTermsComboBox
            // 
            this.payTermsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.payTermsComboBox.FormattingEnabled = true;
            this.payTermsComboBox.Items.AddRange(new object[] {
            "Net 60",
            "Net 30",
            "On Invoice"});
            this.payTermsComboBox.Location = new System.Drawing.Point(263, 99);
            this.payTermsComboBox.Name = "payTermsComboBox";
            this.payTermsComboBox.Size = new System.Drawing.Size(145, 21);
            this.payTermsComboBox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Store Name:";
            // 
            // storeNameBox
            // 
            this.storeNameBox.Location = new System.Drawing.Point(12, 99);
            this.storeNameBox.Name = "storeNameBox";
            this.storeNameBox.ReadOnly = true;
            this.storeNameBox.Size = new System.Drawing.Size(145, 20);
            this.storeNameBox.TabIndex = 29;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(260, 197);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Title:";
            // 
            // titleBox
            // 
            this.titleBox.Location = new System.Drawing.Point(263, 213);
            this.titleBox.Name = "titleBox";
            this.titleBox.ReadOnly = true;
            this.titleBox.Size = new System.Drawing.Size(145, 20);
            this.titleBox.TabIndex = 31;
            // 
            // UpdateOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 297);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.titleBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.storeNameBox);
            this.Controls.Add(this.payTermsComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.updateOrderButton);
            this.Controls.Add(this.titleIDBox);
            this.Controls.Add(this.qtyBox);
            this.Controls.Add(this.orderDateBox);
            this.Controls.Add(this.orderNumBox);
            this.Controls.Add(this.storeIDBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateOrder";
            this.Text = "UpdateOrder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button updateOrderButton;
        private System.Windows.Forms.TextBox titleIDBox;
        private System.Windows.Forms.TextBox qtyBox;
        private System.Windows.Forms.TextBox orderDateBox;
        private System.Windows.Forms.TextBox orderNumBox;
        private System.Windows.Forms.TextBox storeIDBox;
        private System.Windows.Forms.ComboBox payTermsComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox storeNameBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox titleBox;
    }
}