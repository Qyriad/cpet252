﻿namespace MidtermProject
{
    partial class UpdateBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateBook));
            this.pubIDComboBox = new System.Windows.Forms.ComboBox();
            this.ytdBox = new System.Windows.Forms.TextBox();
            this.advanceBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.titleIDBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.updateBookButton = new System.Windows.Forms.Button();
            this.notesBox = new System.Windows.Forms.TextBox();
            this.royaltyBox = new System.Windows.Forms.TextBox();
            this.typeBox = new System.Windows.Forms.TextBox();
            this.titleBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // pubIDComboBox
            // 
            this.pubIDComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pubIDComboBox.FormattingEnabled = true;
            this.pubIDComboBox.Items.AddRange(new object[] {
            "0736",
            "0877",
            "1389",
            "1622",
            "1756",
            "9901",
            "9952",
            "9999"});
            this.pubIDComboBox.Location = new System.Drawing.Point(12, 197);
            this.pubIDComboBox.Name = "pubIDComboBox";
            this.pubIDComboBox.Size = new System.Drawing.Size(144, 21);
            this.pubIDComboBox.TabIndex = 3;
            // 
            // ytdBox
            // 
            this.ytdBox.Location = new System.Drawing.Point(263, 140);
            this.ytdBox.Name = "ytdBox";
            this.ytdBox.Size = new System.Drawing.Size(145, 20);
            this.ytdBox.TabIndex = 7;
            // 
            // advanceBox
            // 
            this.advanceBox.Location = new System.Drawing.Point(263, 27);
            this.advanceBox.Name = "advanceBox";
            this.advanceBox.Size = new System.Drawing.Size(145, 20);
            this.advanceBox.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 244);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 84;
            this.label9.Text = "Price:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 83;
            this.label8.Text = "Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(260, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 82;
            this.label2.Text = "Advance:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 81;
            this.label7.Text = "Title ID:";
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(12, 260);
            this.priceBox.Name = "priceBox";
            this.priceBox.Size = new System.Drawing.Size(145, 20);
            this.priceBox.TabIndex = 4;
            // 
            // titleIDBox
            // 
            this.titleIDBox.Location = new System.Drawing.Point(12, 27);
            this.titleIDBox.Name = "titleIDBox";
            this.titleIDBox.ReadOnly = true;
            this.titleIDBox.Size = new System.Drawing.Size(145, 20);
            this.titleIDBox.TabIndex = 79;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(260, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 78;
            this.label6.Text = "Notes:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(260, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 77;
            this.label5.Text = "YTD Sales:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(260, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 76;
            this.label4.Text = "Royalty:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 75;
            this.label3.Text = "Pub ID:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 74;
            this.label1.Text = "Title:";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(390, 306);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(113, 28);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // updateBookButton
            // 
            this.updateBookButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.updateBookButton.Location = new System.Drawing.Point(271, 306);
            this.updateBookButton.Name = "updateBookButton";
            this.updateBookButton.Size = new System.Drawing.Size(113, 28);
            this.updateBookButton.TabIndex = 9;
            this.updateBookButton.Text = "Update Book";
            this.updateBookButton.UseVisualStyleBackColor = true;
            this.updateBookButton.Click += new System.EventHandler(this.updateBookButton_Click);
            // 
            // notesBox
            // 
            this.notesBox.Location = new System.Drawing.Point(263, 197);
            this.notesBox.Multiline = true;
            this.notesBox.Name = "notesBox";
            this.notesBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.notesBox.Size = new System.Drawing.Size(240, 83);
            this.notesBox.TabIndex = 8;
            // 
            // royaltyBox
            // 
            this.royaltyBox.Location = new System.Drawing.Point(263, 86);
            this.royaltyBox.Name = "royaltyBox";
            this.royaltyBox.Size = new System.Drawing.Size(145, 20);
            this.royaltyBox.TabIndex = 6;
            // 
            // typeBox
            // 
            this.typeBox.Location = new System.Drawing.Point(12, 140);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(145, 20);
            this.typeBox.TabIndex = 2;
            // 
            // titleBox
            // 
            this.titleBox.Location = new System.Drawing.Point(12, 86);
            this.titleBox.Name = "titleBox";
            this.titleBox.Size = new System.Drawing.Size(145, 20);
            this.titleBox.TabIndex = 1;
            // 
            // UpdateBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 355);
            this.Controls.Add(this.pubIDComboBox);
            this.Controls.Add(this.ytdBox);
            this.Controls.Add(this.advanceBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.priceBox);
            this.Controls.Add(this.titleIDBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.updateBookButton);
            this.Controls.Add(this.notesBox);
            this.Controls.Add(this.royaltyBox);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.titleBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateBook";
            this.Text = "UpdateBook";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox pubIDComboBox;
        private System.Windows.Forms.TextBox ytdBox;
        private System.Windows.Forms.TextBox advanceBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.TextBox titleIDBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button updateBookButton;
        private System.Windows.Forms.TextBox notesBox;
        private System.Windows.Forms.TextBox royaltyBox;
        private System.Windows.Forms.TextBox typeBox;
        private System.Windows.Forms.TextBox titleBox;
    }
}