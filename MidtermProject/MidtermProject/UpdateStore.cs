﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.IO;

namespace MidtermProject
{
    public partial class UpdateStore : Form
    {
        Store store;

        string serverIP = Form1.serverIP;
        public UpdateStore(Store s)
        {
            InitializeComponent();
            this.Text = "Update Store";

            store = s;
            storeIDBox.Text = store.stor_id;
            storeNameBox.Text = store.stor_name;
            storeAddressBox.Text = store.stor_address;
            cityBox.Text = store.city;
            stateComboBox.Text = store.state;
            zipBox.Text = store.zip;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //The following function sends the put request to the server
        //The information for the object is taken from the information in the
        //text boxes of the form
        private void updateStoreButton_Click(object sender, EventArgs e)
        {
            Store store = new Store();
            store.stor_id = storeIDBox.Text.Trim();
            store.stor_name = storeNameBox.Text.Trim();
            store.stor_address = storeAddressBox.Text.Trim();
            store.city = cityBox.Text.Trim();
            store.state = stateComboBox.Text.Trim();
            store.zip = zipBox.Text.Trim();

            string endPoint = "http://" + serverIP + "/stores/" + store.stor_id;

            var json = new JavaScriptSerializer().Serialize(store);

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "PUT";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (System.Net.HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

            this.Close();
        }
    }
}
