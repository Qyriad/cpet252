﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//REST client includes

//using System.Net;
//
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json; // be sure to add reference
using Newtonsoft.Json;
using System.Runtime.Serialization; // for [DataContract] attribute
using System.Net;
using System.Globalization;

namespace MidtermProject
{
    public partial class Form1 : Form
    {
        //Store the server IP as a string for easy change between servers
        public static string serverIP = "64.72.2.73:8081";
        //public static string serverIP = "64.72.2.39/rest";
        public Form1()
        {
            //Initialize the form with a set location, name and no resizing
            InitializeComponent();
            this.Text = "Book Order Service";
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.CenterToScreen();

            //Populate the three listviews
            getStores();
            getBooks();
            getOrders();
        }

        public void loadListViewStores()
        {
            //Set up the visuals for the list view.
            lvStores.View = View.Details;
            lvStores.GridLines = true;
            lvStores.FullRowSelect = true;

            //Create columns
            lvStores.Columns.Add("ID");
            lvStores.Columns.Add("Name");
            lvStores.Columns.Add("Address");
            lvStores.Columns.Add("City");
            lvStores.Columns.Add("State");
            lvStores.Columns.Add("Zip Code");

            //Size columns
            lvStores.Columns[0].Width = -2;
            lvStores.Columns[1].Width = -2;
            lvStores.Columns[2].Width = -2;
            lvStores.Columns[3].Width = -2;
            lvStores.Columns[4].Width = -2;
            lvStores.Columns[5].Width = -2;
        }

        public void loadListViewBooks()
        {
            //Set up the visuals for the list view.
            lvBooks.View = View.Details;
            lvBooks.GridLines = true;
            lvBooks.FullRowSelect = true;

            //Create columns
            lvBooks.Columns.Add("ID");
            lvBooks.Columns.Add("Title");
            lvBooks.Columns.Add("Type");
            lvBooks.Columns.Add("Pub ID");
            lvBooks.Columns.Add("Price");
            lvBooks.Columns.Add("Advance");
            lvBooks.Columns.Add("Royalty");
            lvBooks.Columns.Add("Sales");
            lvBooks.Columns.Add("Notes");

            //Size columns
            lvBooks.Columns[0].Width = -2;
            lvBooks.Columns[1].Width = -2;
            lvBooks.Columns[2].Width = -2;
            lvBooks.Columns[3].Width = -2;
            lvBooks.Columns[4].Width = -2;
            lvBooks.Columns[5].Width = -2;
            lvBooks.Columns[6].Width = -2;
            lvBooks.Columns[7].Width = -2;
            lvBooks.Columns[8].Width = -2;
        }

        public void loadListViewOrders()
        {
            //Set up the visuals for the list view.
            lvOrders.View = View.Details;
            lvOrders.GridLines = true;
            lvOrders.FullRowSelect = true;

            //Create columns
            lvOrders.Columns.Add("Store ID");
            lvOrders.Columns.Add("Store");
            lvOrders.Columns.Add("Order Number");
            lvOrders.Columns.Add("Order Date");
            lvOrders.Columns.Add("Quantity");
            lvOrders.Columns.Add("Pay Terms");
            lvOrders.Columns.Add("Title ID");
            lvOrders.Columns.Add("Title");

            //Size columns
            lvOrders.Columns[0].Width = -2;
            lvOrders.Columns[1].Width = -2;
            lvOrders.Columns[2].Width = -2;
            lvOrders.Columns[3].Width = -2;
            lvOrders.Columns[4].Width = -2;
            lvOrders.Columns[5].Width = -2;
            lvOrders.Columns[6].Width = -2;
            lvOrders.Columns[7].Width = -2;
        }

        //The following function gets a list of stores from the server
        //The function sends the request and the server returns json data
        //The json data is then converted in to a list of stores and displayed
        public void getStores()
        {
            //Clear out the list to make sure there's no extra data
            lvStores.Clear();
            string endPoint = "http://" + serverIP + "/stores";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;
                Store[] stores;

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream))
                        {
                            //read in the json value and then convert the json to a store
                            responseValue = reader.ReadToEnd();
                            stores = JsonConvert.DeserializeObject<Store[]>(responseValue);
                            //add the store to a list of stores
                            foreach (var s in stores)
                            {
                                var item = new ListViewItem(s.stor_id);
                                item.SubItems.Add(s.stor_name);
                                item.SubItems.Add(s.stor_address);
                                item.SubItems.Add(s.city);
                                item.SubItems.Add(s.state);
                                item.SubItems.Add(s.zip);

                                lvStores.Items.Add(item);
                            }
                        }
                }
                //load the listview of stores
                loadListViewStores();
            }
        }

        //The following function gets a list of books from the server
        //The function sends the request and the server returns json data
        //The json data is then converted in to a list of books and displayed
        public void getBooks()
        {
            lvBooks.Clear();
            string endPoint = "http://" + serverIP + "/books";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;
                Book[] books;


                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())
                {

                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                            books = JsonConvert.DeserializeObject<Book[]>(responseValue);
                            foreach (var b in books)
                            {
                                var item = new ListViewItem(b.title_id);
                                item.SubItems.Add(b.title);
                                item.SubItems.Add(b.type);
                                item.SubItems.Add(b.pub_id);
                                item.SubItems.Add(b.price);
                                item.SubItems.Add(b.advance);
                                item.SubItems.Add(b.royalty);
                                item.SubItems.Add(b.ytd_sales);
                                item.SubItems.Add(b.notes);
                                item.SubItems.Add(b.pubdate);

                                lvBooks.Items.Add(item);
                            }
                        }
                }
                loadListViewBooks();
            }
        }

        //The following function gets a list of orders from the server
        //The function sends the request and the server returns json data
        //The json data is then converted in to a list of orders and displayed
        public void getOrders()
        {
            lvOrders.Clear();
            string endPoint = "http://" + serverIP + "/sales";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;
                Sales[] sales;

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())
                {

                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                            sales = JsonConvert.DeserializeObject<Sales[]>(responseValue);
                            foreach (var s in sales)
                            {
                                string date = DateTime.ParseExact(s.ord_date, "yyyyMMdd",
                                                CultureInfo.InvariantCulture).ToString("MM/dd/yy");

                                var item = new ListViewItem(s.stor_id);
                                item.SubItems.Add(s.stor_name);
                                item.SubItems.Add(s.ord_num);
                                item.SubItems.Add(date);
                                item.SubItems.Add(s.qty);
                                item.SubItems.Add(s.payterms);
                                item.SubItems.Add(s.title_id);
                                item.SubItems.Add(s.title);

                                lvOrders.Items.Add(item);
                            }
                        }
                }
                loadListViewOrders();
            }
        }

        //The following function watches for an index change in the stores listview
        //If there is a selection change, it will update the orders listview
        //to display the orders of the store that has been selected
        private void lvStores_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvOrders.Clear();

            if (lvStores.SelectedItems.Count > 0)
            {
                string storeId = lvStores.SelectedItems[0].Text;

                if (storeId == null)
                {
                    MessageBox.Show("Please select a store");
                    return;
                }

                else
                {
                    string endPoint = "http://" + serverIP + "/sales/store/" + storeId;

                    var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

                    request.Method = "GET";
                    request.ContentLength = 0;
                    request.ContentType = "text/json";

                    using (var response = (System.Net.HttpWebResponse)request.GetResponse())
                    {
                        var responseValue = string.Empty;
                        Sales[] sales;

                        if (response.StatusCode != System.Net.HttpStatusCode.OK)
                        {
                            throw new ApplicationException(response.StatusCode.ToString());
                        }

                        using (var responseStream = response.GetResponseStream())
                        {

                            if (responseStream != null)
                                using (var reader = new System.IO.StreamReader(responseStream))
                                {
                                    responseValue = reader.ReadToEnd();
                                    sales = JsonConvert.DeserializeObject<Sales[]>(responseValue);
                                    foreach (var s in sales)
                                    {
                                        string date = DateTime.ParseExact(s.ord_date, "yyyyMMdd",
                                                CultureInfo.InvariantCulture).ToString("MM/dd/yy");

                                        var item = new ListViewItem(s.stor_id);
                                        item.SubItems.Add(s.stor_name);
                                        item.SubItems.Add(s.ord_num);
                                        item.SubItems.Add(date);
                                        item.SubItems.Add(s.qty);
                                        item.SubItems.Add(s.payterms);
                                        item.SubItems.Add(s.title_id);
                                        item.SubItems.Add(s.title);

                                        lvOrders.Items.Add(item);
                                    }
                                }
                        }
                        loadListViewOrders();
                    }
                }
            }

            else
                return;
        }

        //The following function watches for an index change in the books listview
        //If there is a selection change, it will update the orders listview
        //to display the orders of the book that has been selected
        private void lvBooks_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvOrders.Clear();

            if (lvBooks.SelectedItems.Count > 0)
            {
                string titleId =lvBooks.SelectedItems[0].Text;
                string endPoint = "http://" + serverIP + "/sales/book/" + titleId;

                var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

                request.Method = "GET";
                request.ContentLength = 0;
                request.ContentType = "text/json";

                using (var response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;
                    Sales[] sales;

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        throw new ApplicationException(response.StatusCode.ToString());
                    }

                    using (var responseStream = response.GetResponseStream())
                    {

                        if (responseStream != null)
                            using (var reader = new System.IO.StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                                sales = JsonConvert.DeserializeObject<Sales[]>(responseValue);
                                foreach (var s in sales)
                                {
                                    string date = DateTime.ParseExact(s.ord_date, "yyyyMMdd",
                                                CultureInfo.InvariantCulture).ToString("MM/dd/yy");

                                    var item = new ListViewItem(s.stor_id);
                                    item.SubItems.Add(s.stor_name);
                                    item.SubItems.Add(s.ord_num);
                                    item.SubItems.Add(date);
                                    item.SubItems.Add(s.qty);
                                    item.SubItems.Add(s.payterms);
                                    item.SubItems.Add(s.title_id);
                                    item.SubItems.Add(s.title);

                                    lvOrders.Items.Add(item);
                                }
                            }
                    }
                    loadListViewOrders();
                }
            }

            else
                return;
        }

        //Sometimes you just want to leave
        private void quitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //This exists to refresh the orders view. Once a book or store has
        //been selected, you must click the orders button to see all of the orders
        //instead of just the selected orders
        private void showOrdersButton_Click(object sender, EventArgs e)
        {
            getOrders();
        }

        //The following function sends a delete request to the server
        private void removeStoreButton_Click(object sender, EventArgs e)
        {

            if (lvStores.SelectedItems.Count > 0)
            {
                string storeId = lvStores.SelectedItems[0].Text;

                //Make sure a store is selected to delete
                if (storeId == null)
                {
                    MessageBox.Show("Please select a store");
                    return;
                }

                //If a store is selected send the delete request to the server
                //based on the store id
                else
                {
                    string endPoint = "http://" + serverIP + "/stores/" + storeId;

                    WebRequest request = WebRequest.Create(endPoint);
                    request.Method = "DELETE";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }

                //Refresh the stores list
                getStores();
            }
        }

        //The following function sends a delete request to the server
        private void removeOrderButton_Click(object sender, EventArgs e)
        {
            if (lvOrders.SelectedItems.Count > 0)
            {
                string orderNum = lvOrders.SelectedItems[0].SubItems[2].Text;

                //Make sure an order is selected to delete
                if (orderNum == null)
                {
                    MessageBox.Show("Please select an order");
                    return;
                }

                //If an order is selected send the delete request to the server
                //based on the order number
                else
                {
                    string endPoint = "http://" + serverIP + "/sales/" + orderNum;

                    WebRequest request = WebRequest.Create(endPoint);
                    request.Method = "DELETE";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }

                getOrders();
            }

            else
            {
                MessageBox.Show("Please select an order");
            }
        }

        //The following function launches a new add order form and
        //passes a store and a book to the form to be used in the creation
        //of the new order
        private void addOrderButton_Click(object sender, EventArgs e)
        {
            if ((lvStores.SelectedItems.Count > 0) && (lvBooks.SelectedItems.Count > 0))
            {
                //Create a new store object based on what is selected
                Store store = new Store();
                store.stor_id = lvStores.SelectedItems[0].Text;
                store.stor_name = lvStores.SelectedItems[0].SubItems[1].Text;
                store.stor_address = lvStores.SelectedItems[0].SubItems[2].Text;
                store.city = lvStores.SelectedItems[0].SubItems[3].Text;
                store.state = lvStores.SelectedItems[0].SubItems[4].Text;
                store.zip = lvStores.SelectedItems[0].SubItems[5].Text;

                //Create a new book object based on what is selected
                Book book = new Book();
                book.title_id = lvBooks.SelectedItems[0].Text;
                book.title = lvBooks.SelectedItems[0].SubItems[1].Text;
                book.type = lvBooks.SelectedItems[0].SubItems[2].Text;
                book.pub_id = lvBooks.SelectedItems[0].SubItems[3].Text;
                book.price = lvBooks.SelectedItems[0].SubItems[4].Text;
                book.advance = lvBooks.SelectedItems[0].SubItems[5].Text;
                book.royalty = lvBooks.SelectedItems[0].SubItems[6].Text;
                book.ytd_sales = lvBooks.SelectedItems[0].SubItems[7].Text;
                book.notes = lvBooks.SelectedItems[0].SubItems[8].Text;
                book.pubdate = lvBooks.SelectedItems[0].SubItems[9].Text;

                //Pass the book and the store to the add order form
                AddOrder newWindow = new AddOrder(store, book);
                //Prepare for a form closing event
                newWindow.FormClosing += new FormClosingEventHandler(AddOrder_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select a store and a book");
                return;
            }
        }

        //The following function launches a new update order form and
        //passes a store and a book to the form to be used in the update
        //of the order
        private void updateOrdersButton_Click(object sender, EventArgs e)
        {
            if (lvOrders.SelectedItems.Count > 0)
            {
                //Create a new sale object to be passed to the form based on what is selected
                Sales order = new Sales();
                order.stor_id = lvOrders.SelectedItems[0].Text;
                order.stor_name = lvOrders.SelectedItems[0].SubItems[1].Text;
                order.ord_num = lvOrders.SelectedItems[0].SubItems[2].Text;
                order.ord_date = lvOrders.SelectedItems[0].SubItems[3].Text;
                order.qty = lvOrders.SelectedItems[0].SubItems[4].Text;
                order.payterms = lvOrders.SelectedItems[0].SubItems[5].Text;
                order.title_id = lvOrders.SelectedItems[0].SubItems[6].Text;
                order.title = lvOrders.SelectedItems[0].SubItems[7].Text;

                UpdateOrder newWindow = new UpdateOrder(order);
                //Prepare for a form closing event
                newWindow.FormClosing += new FormClosingEventHandler(UpdateOrder_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an order");
                return;
            }
        }

        //The following function launches a new add store form
        private void addStoreButton_Click(object sender, EventArgs e)
        {
            AddStore newWindow = new AddStore();
            //Prepare for a form closing event
            newWindow.FormClosing += new FormClosingEventHandler(AddStore_FormClosing);
            newWindow.ShowDialog();
        }

        //The following function launches a new update store form and
        //passes a store to the form to be used in the update
        //of the store
        private void updateStoreButton_Click(object sender, EventArgs e)
        {
            if (lvStores.SelectedItems.Count > 0)
            {
                //Create a new store object to pass base on what is selected
                Store store = new Store();
                store.stor_id = lvStores.SelectedItems[0].Text;
                store.stor_name = lvStores.SelectedItems[0].SubItems[1].Text;
                store.stor_address = lvStores.SelectedItems[0].SubItems[2].Text;
                store.city = lvStores.SelectedItems[0].SubItems[3].Text;
                store.state = lvStores.SelectedItems[0].SubItems[4].Text;
                store.zip = lvStores.SelectedItems[0].SubItems[5].Text;

                UpdateStore newWindow = new UpdateStore(store);
                //Prepare for a form closing event
                newWindow.FormClosing += new FormClosingEventHandler(UpdateStore_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an store");
                return;
            }
        }

        //The following function launches a new add book form
        private void addBookButton_Click(object sender, EventArgs e)
        {
            AddBook newWindow = new AddBook();
            //Prepare for a form closing event
            newWindow.FormClosing += new FormClosingEventHandler(AddBook_FormClosing);
            newWindow.ShowDialog();
        }

        //The following function launches a new update book form and
        //passes a book to the form to be used in the update
        //of the book
        private void updateBookButton_Click(object sender, EventArgs e)
        {
            if (lvBooks.SelectedItems.Count > 0)
            {
                //Create a new book object to be passed based on what is selected
                Book book = new Book();
                book.title_id = lvBooks.SelectedItems[0].Text;
                book.title = lvBooks.SelectedItems[0].SubItems[1].Text;
                book.type = lvBooks.SelectedItems[0].SubItems[2].Text;
                book.pub_id = lvBooks.SelectedItems[0].SubItems[3].Text;
                book.price = lvBooks.SelectedItems[0].SubItems[4].Text;
                book.advance = lvBooks.SelectedItems[0].SubItems[5].Text;
                book.royalty = lvBooks.SelectedItems[0].SubItems[6].Text;
                book.ytd_sales = lvBooks.SelectedItems[0].SubItems[7].Text;
                book.notes = lvBooks.SelectedItems[0].SubItems[8].Text;
                book.pubdate = lvBooks.SelectedItems[0].SubItems[9].Text;

                UpdateBook newWindow = new UpdateBook(book);
                //Prepare for a form closing event
                newWindow.FormClosing += new FormClosingEventHandler(UpdateBook_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select a book");
                return;
            }
        }

        //Update the store list upon the closing of the add storm form
        private void AddStore_FormClosing(object sender, FormClosingEventArgs e)
        {
            getStores();
        }

        //Update the order list upon the closing of the add order form
        private void AddOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            getOrders();
        }

        //Update the book list upon the closing of the add book form
        private void AddBook_FormClosing(object sender, FormClosingEventArgs e)
        {
            getBooks();
        }

        //Update the store list upon the closing of the update store form
        private void UpdateStore_FormClosing(object sender, FormClosingEventArgs e)
        {
            getStores();
        }

        //Update the order list upon the closing of the update order form
        private void UpdateOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            getOrders();
        }

        //Update the book list upon the closing of the update book form
        private void UpdateBook_FormClosing(object sender, FormClosingEventArgs e)
        {
            getBooks();
        }

        //The following function looks for a right mouse click and then displays
        //the corresponding menu
        private void lvStores_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lvStores.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    storesContextMenu.Show(Cursor.Position);
                }
            }
        }

        //The following function launches a new update store form and
        //passes a store to the form to be used in the update
        //of the store
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Create a new store object to pass base on what is selected
            if (lvStores.SelectedItems.Count > 0)
            {
                Store store = new Store();
                store.stor_id = lvStores.SelectedItems[0].Text;
                store.stor_name = lvStores.SelectedItems[0].SubItems[1].Text;
                store.stor_address = lvStores.SelectedItems[0].SubItems[2].Text;
                store.city = lvStores.SelectedItems[0].SubItems[3].Text;
                store.state = lvStores.SelectedItems[0].SubItems[4].Text;
                store.zip = lvStores.SelectedItems[0].SubItems[5].Text;

                UpdateStore newWindow = new UpdateStore(store);
                //Prepare for form closing event
                newWindow.FormClosing += new FormClosingEventHandler(UpdateStore_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an store");
                return;
            }
        }

        //The following function launches a new update book form and
        //passes a book to the form to be used in the update
        //of the book
        private void booksRightClickUpdate_Click(object sender, EventArgs e)
        {
            if (lvBooks.SelectedItems.Count > 0)
            {
                //Create a new book object to be passed based on what is selected
                Book book = new Book();
                book.title_id = lvBooks.SelectedItems[0].Text;
                book.title = lvBooks.SelectedItems[0].SubItems[1].Text;
                book.type = lvBooks.SelectedItems[0].SubItems[2].Text;
                book.pub_id = lvBooks.SelectedItems[0].SubItems[3].Text;
                book.price = lvBooks.SelectedItems[0].SubItems[4].Text;
                book.advance = lvBooks.SelectedItems[0].SubItems[5].Text;
                book.royalty = lvBooks.SelectedItems[0].SubItems[6].Text;
                book.ytd_sales = lvBooks.SelectedItems[0].SubItems[7].Text;
                book.notes = lvBooks.SelectedItems[0].SubItems[8].Text;
                book.pubdate = lvBooks.SelectedItems[0].SubItems[9].Text;

                UpdateBook newWindow = new UpdateBook(book);
                //Prepare for a form closing event
                newWindow.FormClosing += new FormClosingEventHandler(UpdateBook_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select a book");
                return;
            }
        }

        //The following function looks for a right mouse click and then displays
        //the corresponding menu
        private void lvBooks_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lvBooks.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    booksContextMenu.Show(Cursor.Position);
                }
            }
        }

        //The following function launches a new update order form and
        //passes a store and a book to the form to be used in the update
        //of the order
        private void updateOrderRightClick_Click(object sender, EventArgs e)
        {
            if (lvOrders.SelectedItems.Count > 0)
            {
                //Create a new sale object to be passed to the form based on what is selected
                Sales order = new Sales();
                order.stor_id = lvOrders.SelectedItems[0].Text;
                order.stor_name = lvOrders.SelectedItems[0].SubItems[1].Text;
                order.ord_num = lvOrders.SelectedItems[0].SubItems[2].Text;
                order.ord_date = lvOrders.SelectedItems[0].SubItems[3].Text;
                order.qty = lvOrders.SelectedItems[0].SubItems[4].Text;
                order.payterms = lvOrders.SelectedItems[0].SubItems[5].Text;
                order.title_id = lvOrders.SelectedItems[0].SubItems[6].Text;
                order.title = lvOrders.SelectedItems[0].SubItems[7].Text;

                UpdateOrder newWindow = new UpdateOrder(order);
                //Prepare for a form closing event
                newWindow.FormClosing += new FormClosingEventHandler(UpdateOrder_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an order");
                return;
            }
        }

        //The following function sends a delete request to the server
        private void removeOrderRightClick_Click(object sender, EventArgs e)
        {
            if (lvOrders.SelectedItems.Count > 0)
            {
                string orderNum = lvOrders.SelectedItems[0].SubItems[2].Text;

                //Make sure an order is selected to delete
                if (orderNum == null)
                {
                    MessageBox.Show("Please select an order");
                    return;
                }

                //If an order is selected send the delete request to the server
                //based on the order number
                else
                {
                    string endPoint = "http://" + serverIP + "/sales/" + orderNum;

                    WebRequest request = WebRequest.Create(endPoint);
                    request.Method = "DELETE";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }

                getOrders();
            }

            else
            {
                MessageBox.Show("Please select an order");
            }
        }

        //The following function looks for a right mouse click and then displays
        //the corresponding menu
        private void lvOrders_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lvOrders.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    ordersContextMenu.Show(Cursor.Position);
                }
            }
        }

        //The following function launches a new add store form
        private void storesRightClickAdd_Click(object sender, EventArgs e)
        {
            AddStore newWindow = new AddStore();
            //Prepare for a form closing event
            newWindow.FormClosing += new FormClosingEventHandler(AddStore_FormClosing);
            newWindow.ShowDialog();
        }

        //The following function launches a new add book form
        private void booksRightClickAdd_Click(object sender, EventArgs e)
        {
            AddBook newWindow = new AddBook();
            //Prepare for a form closing event
            newWindow.FormClosing += new FormClosingEventHandler(AddBook_FormClosing);
            newWindow.ShowDialog();
        }

        //This exists to refresh the orders view. Once a book or store has
        //been selected, you must click the orders button to see all of the orders
        //instead of just the selected orders
        private void showOrdersRightClick_Click(object sender, EventArgs e)
        {
            getOrders();
        }

        //The following function launches a new add order form and
        //passes a store and a book to the form to be used in the creation
        //of the new order
        private void addOrderRightClick_Click(object sender, EventArgs e)
        {
            if ((lvStores.SelectedItems.Count > 0) && (lvBooks.SelectedItems.Count > 0))
            {
                //Create a new store object based on what is selected
                Store store = new Store();
                store.stor_id = lvStores.SelectedItems[0].Text;
                store.stor_name = lvStores.SelectedItems[0].SubItems[1].Text;
                store.stor_address = lvStores.SelectedItems[0].SubItems[2].Text;
                store.city = lvStores.SelectedItems[0].SubItems[3].Text;
                store.state = lvStores.SelectedItems[0].SubItems[4].Text;
                store.zip = lvStores.SelectedItems[0].SubItems[5].Text;

                //Create a new book object based on what is selected
                Book book = new Book();
                book.title_id = lvBooks.SelectedItems[0].Text;
                book.title = lvBooks.SelectedItems[0].SubItems[1].Text;
                book.type = lvBooks.SelectedItems[0].SubItems[2].Text;
                book.pub_id = lvBooks.SelectedItems[0].SubItems[3].Text;
                book.price = lvBooks.SelectedItems[0].SubItems[4].Text;
                book.advance = lvBooks.SelectedItems[0].SubItems[5].Text;
                book.royalty = lvBooks.SelectedItems[0].SubItems[6].Text;
                book.ytd_sales = lvBooks.SelectedItems[0].SubItems[7].Text;
                book.notes = lvBooks.SelectedItems[0].SubItems[8].Text;
                book.pubdate = lvBooks.SelectedItems[0].SubItems[9].Text;

                //Pass the book and the store to the add order form
                AddOrder newWindow = new AddOrder(store, book);
                //Prepare for form closing event
                newWindow.FormClosing += new FormClosingEventHandler(AddOrder_FormClosing);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select a store and a book");
                return;
            }
        }
    }

    //Definition of classes below
    [DataContract]
    public class Store
    {
        [DataMember]
        public string stor_id { get; set; }
        [DataMember]
        public string stor_name { get; set; }
        [DataMember]
        public string stor_address { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string zip { get; set; }
    }

    [DataContract]
    public class Sales
    {
        [DataMember]
        public string stor_name { get; set; }
        [DataMember]
        public string stor_id { get; set; }
        [DataMember]
        public string ord_num { get; set; }
        [DataMember]
        public string ord_date { get; set; }
        [DataMember]
        public string qty { get; set; }
        [DataMember]
        public string payterms { get; set; }
        [DataMember]
        public string title_id { get; set; }
        [DataMember]
        public string title { get; set; }
    }

    [DataContract]
    public class Author
    {
        [DataMember]
        public string au_id { get; set; }
        [DataMember]
        public string au_lname { get; set; }
        [DataMember]
        public string au_fname { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string zip { get; set; }
        [DataMember]
        public string contract { get; set; }
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public string title_id { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string pub_id { get; set; }
        [DataMember]
        public string price { get; set; }
        [DataMember]
        public string advance { get; set; }
        [DataMember]
        public string royalty { get; set; }
        [DataMember]
        public string ytd_sales { get; set; }
        [DataMember]
        public string notes { get; set; }
        [DataMember]
        public string pubdate { get; set; }
    }
}
