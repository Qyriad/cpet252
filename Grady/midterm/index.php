<?php

require 'flight-master/flight/Flight.php';

Flight::register('pubs_db', 'PDO', array('mysql:host=localhost;port=8081;dbname=pubs', 'root', 'root') 

	); 

 // LIST BOOKS
function get_books() { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT * FROM titles"; 

	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

 // UPDATE BOOKS
function put_books($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "UPDATE titles SET title=?, type=?, pub_id=?, price=?, advance=?, royalty=?, ytd_sales=?, notes=? WHERE title_id=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes, $title_id)); 
		
		//$rows = array();
		
		 echo "Record updated successfully";
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

 // ADD BOOKS
function post_books($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes) {

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "INSERT titles VALUES (?,?,?,?,?,?,?,?,?)"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes)); 
		
		$rows = array(); 

		echo "Record added successfully";

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

 // DELETE BOOKS
function delete_books($title_id) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "DELETE FROM titles WHERE title_id=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($title_id));
		//$rows = array(); 

		/*while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		echo $row_json;
		*/

		echo "Record deleted successfully";
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

//
// STORES
//

 // LIST STORES
function get_stores() { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT * FROM stores"; 

	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

 // UPDATE STORES
function put_stores($stor_id, $stor_name, $stor_address, $city, $state, $zip) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "UPDATE stores SET stor_name=?, stor_address=?, city=?, state=?, zip=? WHERE stor_id=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($stor_name, $stor_address, $city, $state, $zip, $stor_id)); 
		
		//$rows = array();
		
		 echo "Record updated successfully";
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}
// ADD STORES
function post_stores($stor_id, $stor_name, $stor_address, $city, $state, $zip) {

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "INSERT stores VALUES (?,?,?,?,?,?)"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($stor_id, $stor_name, $stor_address, $city, $state, $zip)); 
		
		$rows = array(); 

		echo "Record added successfully";

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

// DELETE STORES
function delete_stores($stor_id) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "DELETE FROM stores WHERE stor_id=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($stor_id));
		//$rows = array($title_id); 

		/*while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		echo $row_json;
		*/

		echo "Record deleted successfully";
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}
//
// SALES
//

// LIST SALES
function get_sales() { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT stores.stor_id, stores.stor_name, sales.ord_num, sales.ord_date, sales.qty, sales.payterms, titles.title_id, titles.title FROM sales JOIN stores ON sales.stor_id=stores.stor_id JOIN titles ON sales.title_id=titles.title_id"; 

	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

// LIST SALES BY STORE
function get_sales_by_store($stor_id) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT stores.stor_id, stores.stor_name, sales.ord_num, sales.ord_date, sales.qty, 		sales.payterms, titles.title_id, titles.title FROM stores JOIN sales ON 		sales.stor_id=stores.stor_id JOIN titles ON sales.title_id=titles.title_id WHERE stores.stor_id=?"; 


	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(array($stor_id)); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

// LIST SALES BY TITLE
function get_sales_by_title($title_id) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT stores.stor_id, stores.stor_name, sales.ord_num, sales.ord_date, sales.qty, sales.payterms, titles.title_id, titles.title FROM stores JOIN sales ON sales.stor_id=stores.stor_id JOIN titles ON sales.title_id=titles.title_id WHERE titles.title_id=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(array($title_id)); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

// UPDATE SALES
function put_sales($stor_id, $ord_num, $ord_date, $qty, $payterms, $title_id) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "UPDATE sales SET stor_id=?, ord_date=?, qty=?, payterms=?, title_id=? WHERE ord_num=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($stor_id, $ord_date, $qty, $payterms, $title_id, $ord_num)); 
		
		//$rows = array($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes);
		
		 echo "Record updated successfully";
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

// ADD SALES
function post_sales($stor_id, $ord_num, $ord_date, $qty, $payterms, $title_id) {

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "INSERT sales VALUES (?,?,?,?,?,?)"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($stor_id, $ord_num, $ord_date, $qty, $payterms, $title_id)); 
		
		$rows = array(); 

		echo "Record added successfully";

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

// DELETE SALES
function delete_sales($ord_num) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, true);

	$sql = "DELETE FROM sales WHERE ord_num=?"; 

	$stmt = $handle->prepare($sql);

        try{

		$stmt->execute(array($ord_num));
		//$rows = array($title_id); 

		/*while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		echo $row_json;
		*/

		echo "Record deleted successfully";
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

//
// BOOKS
//

Flight::route('GET /books', function() { 

	get_books(); // LIST BOOKS
}
);

Flight::route('PUT /books/@titleid', function($titleid) { 

	$title_id = Flight::request()->data->title_id;
	$title = Flight::request()->data->title;
	$type = Flight::request()->data->type;
	$pub_id = Flight::request()->data->pub_id;
	$price = Flight::request()->data->price;
	$advance= Flight::request()->data->advance;
	$royalty = Flight::request()->data->royalty;
	$ytd_sales = Flight::request()->data->ytd_sales;
	$notes = Flight::request()->data->notes;	

	put_books($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes); // UPDATE BOOKS
} 
);

Flight::route('POST /books', function() {

	$title_id = Flight::request()->data->title_id;
	$title = Flight::request()->data->title;
	$type = Flight::request()->data->type;
	$pub_id = Flight::request()->data->pub_id;
	$price = Flight::request()->data->price;
	$advance= Flight::request()->data->advance;
	$royalty = Flight::request()->data->royalty;
	$ytd_sales = Flight::request()->data->ytd_sales;
	$notes = Flight::request()->data->notes;

	post_books($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes); // ADD BOOKS
 
} 
);  

Flight::route('DELETE /books/@titleid', function($titleid) { 

	//$title_id = Flight::request()->data->title_id; 

	delete_books($titleid); // DELETE BOOKS
} 
); 

//
// STORES
//

Flight::route('GET /stores', function() { 

	get_stores(); // LIST STORES
}
); 

Flight::route('PUT /stores/@storid', function($storid) { 

	$stor_id = Flight::request()->data->stor_id;
	$stor_name = Flight::request()->data->stor_name;
	$stor_address = Flight::request()->data->stor_address;
	$city = Flight::request()->data->city;
	$state = Flight::request()->data->state;
	$zip = Flight::request()->data->zip;	

	put_stores($stor_id, $stor_name, $stor_address, $city, $state, $zip); // UPDATE STORES
} 
);

Flight::route('POST /stores', function() {

	$stor_id = Flight::request()->data->stor_id;
	$stor_name = Flight::request()->data->stor_name;
	$stor_address = Flight::request()->data->stor_address;
	$city = Flight::request()->data->city;
	$state = Flight::request()->data->state;
	$zip = Flight::request()->data->zip;	

	post_stores($stor_id, $stor_name, $stor_address, $city, $state, $zip); // ADD STORES
 
} 
); 

Flight::route('DELETE /stores/@storid', function($storid) { 

	//$stor_id = Flight::request()->data->stor_id; 

	delete_stores($storid); // DELETE STORES
} 
); 

//
// SALES
//

Flight::route('GET /sales', function() { 

	get_sales(); // LIST SALES
}
);
Flight::route('GET /sales/store/@storid', function($storid) { 

	get_sales_by_store($storid); // LIST SALES
}
);
Flight::route('GET /sales/book/@titleid', function($titleid) { 

	get_sales_by_title($titleid); // LIST SALES
}
);

Flight::route('PUT /sales/@ordnum', function($ordnum) { 

	$stor_id = Flight::request()->data->stor_id;
	$ord_num = Flight::request()->data->ord_num;
	$ord_date = Flight::request()->data->ord_date;
	$qty = Flight::request()->data->qty;
	$payterms = Flight::request()->data->payterms;
	$title_id= Flight::request()->data->title_id;	

	put_sales($stor_id, $ord_num, $ord_date, $qty, $payterms, $title_id); // UPDATE STORES
} 
);

Flight::route('POST /sales', function() {

	$stor_id = Flight::request()->data->stor_id;
	$ord_num = Flight::request()->data->ord_num;
	$ord_date = Flight::request()->data->ord_date;
	$qty = Flight::request()->data->qty;
	$payterms = Flight::request()->data->payterms;
	$title_id= Flight::request()->data->title_id;	


	post_sales($stor_id, $ord_num, $ord_date, $qty, $payterms, $title_id); // ADD STORES
} 
);

Flight::route('DELETE /sales/@ordnum', function($ordnum) { 

	//$ord_num = Flight::request()->data->ord_num; 

	delete_sales($ordnum); // DELETE SALES
} 
); 

Flight::start(); 

?>