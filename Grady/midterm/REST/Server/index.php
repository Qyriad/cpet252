<?php

require 'flight-master/flight/Flight.php';

Flight::register('pubs_db', 'PDO', array('mysql:host=localhost;port=3306;dbname=pubs', 'root', 'root') 

	); 

function get_authors() { 


	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT * FROM authors"; 

	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

function get_author($id) { 

	$handle = Flight::pubs_db(); 

	$handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);

	$sql = "SELECT * FROM authors WHERE au_id = ?"; 

	$stmt = $handle->prepare($sql);

        try{

		$result = $stmt->execute(array($id)); 
		
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row;
		}

		$row_json = json_encode($rows); 
		
		echo $row_json; 
	 

	}catch(PDOException $e) {

  		echo "error " . $e->getMessage();
	}

	$stmt->closeCursor(); 
 
	$handle = null; 
	$stmt = null; 
}

Flight::route('GET /authors(/@auid)', function($auid) { 

	if($auid) get_author($auid);  
	else get_authors(); 
} 
); 

Flight::start(); 

?>


