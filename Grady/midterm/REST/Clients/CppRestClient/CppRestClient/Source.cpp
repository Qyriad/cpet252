#include <iostream>
#include <map>
#include <string>
using namespace std;

#include <windows.h>
#include <winhttp.h>

#pragma comment(lib, "winhttp.lib")

HANDLE eventWaitForHeader; 

VOID CALLBACK AsyncCallback( HINTERNET hInternet,
	 DWORD_PTR dwContext,
	DWORD dwInternetStatus,
	LPVOID lpvStatusInformation, // OPTIONAL,
	 DWORD dwStatusInformationLength)
{
	switch (dwInternetStatus)
	{
	case WINHTTP_CALLBACK_STATUS_RESOLVING_NAME:
		printf("resolving name\n");
		break;
	case WINHTTP_CALLBACK_STATUS_NAME_RESOLVED:
		printf("name resolved\n");
		break;
	case WINHTTP_CALLBACK_STATUS_CONNECTING_TO_SERVER:
		printf("connecting to server\n");
		break;
	case WINHTTP_CALLBACK_STATUS_SENDING_REQUEST:
		printf("sending request\n");
		break;
	case WINHTTP_CALLBACK_STATUS_REQUEST_SENT:
		printf("request sent\n");
		break;
	case WINHTTP_CALLBACK_STATUS_HANDLE_CREATED:
		printf("Handle created\n");
		break;
	case WINHTTP_CALLBACK_STATUS_CONNECTED_TO_SERVER:
		printf("connected\n");
		break;
	case WINHTTP_CALLBACK_STATUS_CONNECTION_CLOSED:
		printf("connection closed\n");
		break;
	case WINHTTP_CALLBACK_STATUS_RESPONSE_RECEIVED:
	{
		DWORD * pBytes_received = (DWORD *)lpvStatusInformation;

		printf("response received.  %d bytes \n", *pBytes_received);
	}
	break;
	case WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE:
	{
	printf("headers avaialable\n");
	DWORD dwSize = 0; 
	WinHttpQueryHeaders(hInternet, WINHTTP_QUERY_RAW_HEADERS_CRLF,
		WINHTTP_HEADER_NAME_BY_INDEX, NULL,
		&dwSize, WINHTTP_NO_HEADER_INDEX);

	// Allocate memory for the buffer.
	if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
	{
		LPVOID lpOutBuffer = new WCHAR[dwSize / sizeof(WCHAR)];

		// Now, use WinHttpQueryHeaders to retrieve the header.
		BOOL bResults = WinHttpQueryHeaders(hInternet,
			WINHTTP_QUERY_RAW_HEADERS_CRLF,
			WINHTTP_HEADER_NAME_BY_INDEX,
			lpOutBuffer, &dwSize,
			WINHTTP_NO_HEADER_INDEX);

		if (bResults)
		{
			printf("Header contents: \n%S", lpOutBuffer);

			// Free the allocated memory.
			
		}

		delete[] lpOutBuffer;
	}
	}// end case

		SetEvent(eventWaitForHeader); 

		break;
	case WINHTTP_CALLBACK_STATUS_DATA_AVAILABLE: 
	{
		printf("data ready to read\n");

		DWORD * pSize = (DWORD *)lpvStatusInformation;
		CHAR * message = new CHAR[*pSize]; 
		DWORD bytes_read; 
		WinHttpReadData(
			hInternet, // handle returned on open, not sure if this is it
			(PCHAR)message,
			*pSize,
			&bytes_read
		);

		printf("bytes to read %d, bytes read %d", *pSize, bytes_read); 

		for (int i = 0; i < bytes_read; i++)
		{
			printf("%c", message[i]); 
		}


	}
		break;
	case WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE:
		printf("request complete\n");
		break;
	case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR: {
		//An error occurred while sending an HTTP request.
	// The lpvStatusInformation parameter contains a pointer to a WINHTTP_ASYNC_RESULT structure.Its dwResult member indicates the ID of the called function and dwError indicates the return value.
		WINHTTP_ASYNC_RESULT * result = (WINHTTP_ASYNC_RESULT *)lpvStatusInformation;
		printf("error result %d, %x : %d, %x\n", result->dwError, result->dwError, result->dwResult, result->dwResult);
	}
												break; 
	case WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING:
		printf("handle closeing\n");
		break;
	default:
		printf("default %x\n", dwInternetStatus);
		break;
	}
}

int main()
{
	// The event is global to indicate when the header has finished reading
	eventWaitForHeader = CreateEvent(0, FALSE, FALSE, NULL); 

	BOOL  bResults = FALSE;
	HINTERNET hSession = NULL,
		hConnect = NULL,
		hRequest = NULL;

	WCHAR name[128] = { 0 };

	GetModuleFileName(
		NULL,
		name,
		128
	);

	// Use WinHttpOpen to obtain a session handle.
	// WINHTTP_FLAG_ASYNC
	hSession = WinHttpOpen(name,
		WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME,
		WINHTTP_NO_PROXY_BYPASS, WINHTTP_FLAG_ASYNC); //  last param was 0);

	WINHTTP_STATUS_CALLBACK isCallback = WinHttpSetStatusCallback(hSession,
		(WINHTTP_STATUS_CALLBACK)AsyncCallback,
		WINHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS,
		NULL);

	// Specify an HTTP server.
	if (hSession)
		hConnect = WinHttpConnect(hSession, L"64.72.1.140",
			INTERNET_DEFAULT_HTTP_PORT, 0);
	if (!hSession)
		printf("hSession Error %d has occurred.\n", GetLastError());
	// Create an HTTP Request handle.
	if (hConnect)
		hRequest = WinHttpOpenRequest(hConnect, L"GET",
			L"/rest/authors",
			NULL, WINHTTP_NO_REFERER,
			WINHTTP_DEFAULT_ACCEPT_TYPES,
			0);
	if (!hConnect)
		printf("hConnect Error %d has occurred.\n", GetLastError());
	// Send a Request.

	// the error is here with send request
	if (hRequest)
		bResults = WinHttpSendRequest(hRequest,
			WINHTTP_NO_ADDITIONAL_HEADERS,
			0, WINHTTP_NO_REQUEST_DATA, 0,
			0, 0);

	// Report any errors.
	if (!hRequest)
		printf("Request Error %d has occurred.\n", GetLastError());

	
	// PLACE ADDITIONAL CODE HERE.
	BOOL bReceive =  WinHttpReceiveResponse(
		hRequest,
		NULL
	);
	if (!bReceive)
		printf("Recieve Error %d has occurred.\n", GetLastError());

	// You can not call query data until the handle has finished reading 
	WaitForSingleObject(eventWaitForHeader, INFINITE); 

	BOOL bDataAvailable = WinHttpQueryDataAvailable(
		hRequest,
		NULL
	);

	if (!bDataAvailable) {
		DWORD err = GetLastError(); 
		printf("Data available Error %d has occurred.\n", err);
	}


	system("pause"); 

	// Close any open handles.
	if (hRequest) WinHttpCloseHandle(hRequest);
	if (hConnect) WinHttpCloseHandle(hConnect);
	if (hSession) WinHttpCloseHandle(hSession);

	CloseHandle(eventWaitForHeader); 

	return 0; 
}