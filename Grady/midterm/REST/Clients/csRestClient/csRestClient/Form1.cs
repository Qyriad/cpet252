﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// REST client includes 

//using System.Net;
//
using System.Net.Http;
using System.Net.Http.Headers; 
using System.Runtime.Serialization.Json; // be sure to add reference

namespace csRestClient
{
    /*
     adapted from: https://www.codeproject.com/tips/497123/how-to-make-rest-requests-with-csharp
         */
    public partial class Form1 : Form
    {
        public enum HttpVerb { GET,POST, PUT, DELETE }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string endPoint = "http://64.72.1.140/rest/authors";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET"; 
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())  {
                var responseValue = string.Empty;

                if (response.StatusCode != System.Net.HttpStatusCode.OK)  {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())  {
                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream)) {
                            responseValue = reader.ReadToEnd();
                        }
                }

                textBox1.Text = responseValue;
            }
        }
    }
}
