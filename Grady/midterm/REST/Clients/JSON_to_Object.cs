﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// required headers

// Right click References and select Add Reference
// In the Framework list, select: 
// System.Runtime.Serialization
// You can then use the Json namespace: 
using System.Runtime.Serialization.Json;

using System.Runtime.Serialization; // for [DataContract] attribute
using System.IO;                    // for MemoryStream 

namespace JSONw_DotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            // the JSON string requires quotes, so use \ to escape the quotes and 
            // allow insertion into the string
            string json = "{\"name\":\"ken\", \"age\":\"33\"}"; 

            person x = new person();

            // convert json string into a byte stream
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));

            // perpare to convert json into an object 
            DataContractJsonSerializer ser = new DataContractJsonSerializer(x.GetType());

            // the serializer reads the memory stream and returns an object
            x = ser.ReadObject(ms) as person;

            ms.Close(); // done with memory stream 

            Console.WriteLine("{0} : {1}", x.name, x.age); 

        }
    }

    [DataContract]
    class person
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string age { get; set;  }
    }
}
