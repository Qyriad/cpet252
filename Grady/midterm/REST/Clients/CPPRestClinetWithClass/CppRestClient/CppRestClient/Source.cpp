#include <iostream>
#include <map>
#include <string>
#include <exception>
using namespace std;

#include <windows.h>
#include <winhttp.h>

#include <wchar.h>
#include <corecrt_wstring.h>

#pragma comment(lib, "winhttp.lib")

typedef map<wstring, wstring> http_header; 
typedef pair<wstring, wstring> http_header_item; 

class http_exception : public std::runtime_error {
private:
	DWORD _error;

public:
	http_exception(char * what) : std::runtime_error(what)
	{
		_error = GetLastError(); 
	}

	http_exception(char * what, DWORD error) : std::runtime_error(what)
	{
		_error = error;
	}

	DWORD error() { return _error;  }
};

class http_request {
private:

	HINTERNET _hSession = NULL,
		_hConnect = NULL,
		_hRequest = NULL;

	WCHAR * _accept_types; 

	http_header _header;

	DWORD _bytes_read; 

	char * _message; 

	HINTERNET http_open()
	{
		WCHAR name[128] = { 0 };

		GetModuleFileName(NULL, name, 128);

		DWORD flags = 0;

		HINTERNET session = WinHttpOpen(name,
			WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
			WINHTTP_NO_PROXY_NAME,
			WINHTTP_NO_PROXY_BYPASS, flags); 

		return session;
	}

	HINTERNET http_connect(HINTERNET & session, WCHAR * host)
	{
		return WinHttpConnect(session, host, INTERNET_DEFAULT_HTTP_PORT, 0);
	}

	BOOL http_recv_response(HINTERNET & request)
	{
		return WinHttpReceiveResponse(request, NULL);
	}

	BOOL http_query_headers(HINTERNET request, LPVOID buffer, DWORD * size)
	{
		return WinHttpQueryHeaders(request, WINHTTP_QUERY_RAW_HEADERS_CRLF,
			WINHTTP_HEADER_NAME_BY_INDEX, buffer,
			size, WINHTTP_NO_HEADER_INDEX);
	}

	bool parse_header(http_header & header, WCHAR * header_out_buffer)
	{
		WCHAR * next_tok = 0;
		WCHAR * next_val;
		WCHAR * next_http;

		WCHAR * str = wcstok_s(header_out_buffer, L"\r\n", &next_tok); // c is ptr to crlf 

		WCHAR * http1_1 = wcstok_s(str, L" ", &next_http);
		WCHAR * code = wcstok_s(NULL, L" ", &next_http);
		WCHAR * txt_code = wcstok_s(NULL, L"\r", &next_http);

		header[wstring(L"response_code")] = wstring(code);
		header[wstring(L"response_text")] = wstring(txt_code);

		while (str)
		{
			str = wcstok_s(NULL, L"\r\n", &next_tok); // c is ptr to crlf 

			printf("The string is: %S\n", str);

			WCHAR * key = wcstok_s(str, L":", &next_val); // c is ptr to crlf 
			WCHAR * value = wcstok_s(NULL, L"\r", &next_val);

			if (key && value)
			{
				header[wstring(key)] = wstring(value);
			}
		}

		return true;
	}

	BOOL receive_headers(HINTERNET request, http_header & header)
	{
		DWORD dwSize = 0;
		BOOL result = http_query_headers(request, NULL, &dwSize); 

		// Allocate memory for the buffer.
		if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
		{
			LPVOID lpOutBuffer = new WCHAR[dwSize / sizeof(WCHAR)];

			// Now, use WinHttpQueryHeaders to retrieve the header.
			BOOL bResults = http_query_headers(request, lpOutBuffer, &dwSize); 

			parse_header(header, (WCHAR *)lpOutBuffer);

			for (http_header_item item : header)
			{
				printf("header info ... %S : %S\n", item.first.c_str(), item.second.c_str());
			}

			delete[] lpOutBuffer;

			return TRUE; 

			/*
			DWORD data_available = 0;
			wstring len = header[L"Content-Length"];
			DWORD data_length = _wtoi(len.c_str());

			cout << "ready to read " << data_length << " data" << endl;

			CHAR * message = new CHAR[data_length];

			BOOL data_result = WinHttpQueryDataAvailable(
				hRequest,
				&data_available
			);

			if (!data_result)
			{
				delete[] message;
				return false;
			}

			DWORD bytes_read;
			BOOL bReadBytes = WinHttpReadData(
				hRequest,
				(PCHAR)message,
				data_length,
				&bytes_read
			);

			cout << "read " << bytes_read << " bytes" << endl;

			if (!bReadBytes)
			{
				delete[] message;
				return false;
			}
			*/ 
		}// end if 

		return FALSE; 
	}

public:

	enum VERB {
		PUT,
		GET,
		POST,
		DEL
	};

	WCHAR * verb_to_string(VERB verb)
	{
		switch (verb)
		{
		case PUT: return L"PUT";
		case GET: return L"GET";
		case POST: return L"POST";
		case DEL: return L"DELETE"; 
		}
	}

	struct _header_fields_ {
		const WCHAR * Connection = L"Connection";
		const WCHAR * Content_Length = L"Content-Length";
		const WCHAR * Content_Type = L"Content-Type";
		const WCHAR * Date = L"Date";
		const WCHAR * Keep_Alive = L"Keep-Alive";
		const WCHAR * Server = L"Server";
		const WCHAR * X_Powered_By = L"X-Powered-By";
		const WCHAR * Response_Code = L"response_code";
		const WCHAR * Response_Text = L"response_text";
	}header_fields;

	http_request()
	{
		_bytes_read = 0; 
		_accept_types = WINHTTP_DEFAULT_ACCEPT_TYPES;
		_message = nullptr; 
	}

	~http_request()
	{
		if (_hRequest) WinHttpCloseHandle(_hRequest);
		if (_hConnect) WinHttpCloseHandle(_hConnect);
		if (_hSession) WinHttpCloseHandle(_hSession);

		if (_message) delete[]  _message; 
	}

	void connect(WCHAR * host)
	{
		_hSession = this->http_open(); 
		if (!_hSession) throw http_exception("http_open"); 

		_hConnect = http_connect(_hSession, host); 
		if(!_hConnect)  throw http_exception("http_connect");
	}

	void send(VERB verb, WCHAR * resource) // , WCHAR * message = nullptr)
	{
		if (_hConnect == NULL) throw http_exception("not connected", 0); 

		_hRequest = WinHttpOpenRequest(_hConnect, verb_to_string(verb), resource,
			NULL, WINHTTP_NO_REFERER,
			(LPCWSTR *)&_accept_types,
			0);

		if(!_hRequest) throw http_exception("http_open_request");

		BOOL bResults = WinHttpSendRequest(_hRequest,
			WINHTTP_NO_ADDITIONAL_HEADERS,
			0, WINHTTP_NO_REQUEST_DATA, 0,
			0, 0);

		if(!bResults) throw http_exception("http_send_request");

		if(http_recv_response(_hRequest) == FALSE)  throw http_exception("http_recv_request");

		if(receive_headers(_hRequest, _header) == FALSE) throw http_exception("http_recv_headers");
	}

	void send(VERB verb, WCHAR * resource, WCHAR * message, DWORD msg_length)
	{
		if (_hConnect == NULL) throw http_exception("not connected", 0);

		_hRequest = WinHttpOpenRequest(_hConnect, verb_to_string(verb), resource,
			NULL, WINHTTP_NO_REFERER,
			(LPCWSTR *)&_accept_types,
			0);

		if (!_hRequest) throw http_exception("http_open_request");

		char msg[32] = "{\"name\":\"hello\"}";

		BOOL bResults = WinHttpSendRequest(_hRequest,
			WINHTTP_NO_ADDITIONAL_HEADERS,
			0, msg, strlen(msg),
			strlen(msg), 0);

		if (!bResults) throw http_exception("http_send_request");

		if (http_recv_response(_hRequest) == FALSE)  throw http_exception("http_recv_request");

		if (receive_headers(_hRequest, _header) == FALSE) throw http_exception("http_recv_headers");
	}

	void read()
	{
		wstring len = _header[L"Content-Length"]; 
		DWORD data_length = _wtoi(len.c_str());

		_message = new CHAR[data_length];

		DWORD data_available = 0; 

		BOOL data_result = WinHttpQueryDataAvailable(
			_hRequest,
			&data_available
		);

		if (!data_result)
		{
			delete[] _message;
			_message = nullptr; 
			throw http_exception("query_data_available"); 
		}

		DWORD bytes_read;
		BOOL bReadBytes = WinHttpReadData(
			_hRequest,
			(PCHAR)_message,
			data_length,
			&bytes_read
		);

		_bytes_read = bytes_read; 

		if (!bReadBytes)
		{
			delete[] _message; 
			_message = nullptr; 
			throw http_exception("read_data");
		}
	}

	DWORD bytes_read()
	{
		return _bytes_read; 
	}

	const char * message() const
	{
		return _message; 
	}

	http_header header() const
	{
		return _header; 
	}
};

int main()
{
	try {

		http_request request;

		request.connect(L"64.72.16.182");

		//request.send(http_request::VERB::GET, L"/rest/authors");

		request.send(http_request::VERB::PUT, L"/rest/authors/1234", L"{au_lname:ken}", 7);

		if (request.header()[request.header_fields.Response_Text] != L"OK")
		{
			cout << "failed get data" << endl; 
			return 1; 
		} 
		request.read();

		const char * message = request.message(); 

		if (!message)
		{
			cout << "no message" << endl;
			return 0; 
		}

		for (int i = 0; i < request.bytes_read(); i++)
		{
			cout << message[i]; 
		}
	}
	catch (http_exception ex) {
		cout << "exception " << ex.what() << " code " << ex.error() << endl; 

		switch (ex.error())
		{
		case ERROR_WINHTTP_CONNECTION_ERROR:
			break; 
		}
	}

	return 0; 
}

