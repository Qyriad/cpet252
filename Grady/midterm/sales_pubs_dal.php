<?php







function connect_to_pubs()



{



	$dsn = 'mysql:host=localhost;port=3306;dbname=pubs'; 



	$user = 'root';



	$password = 'root'; 







	$handle = new PDO($dsn, $user, $password); 



	$handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 







	return $handle; 



}







function get_all_titles()



{



	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}











	$sql = "SELECT * FROM titles"; //'CALL get_all_titles()'; // stored procedure 







	$proc_get_titles = $conn->prepare($sql);



	



	try{







		$rs = $proc_get_titles->execute(); // result set = sql query 



	}catch(PDOException $ex){



		$conn = null; // close connection 



		return "Bad sql";



	}







	$rows = array(); 







	while($row = $proc_get_titles->fetch(PDO::FETCH_ASSOC)){



		$rows[] = $row; // add row to array



	}







	$retVal = json_encode($rows); 



	$conn = null; // close connection 







	return $retVal; 







}



/*



function get_all_stores()



{



	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}











	$sql = "SELECT * FROM stores"; //'CALL get_all_stores()'; // stored procedure 







	$proc_get_stores = $conn->prepare($sql);



	



	try{







		$rs = $proc_get_stores->execute(); // result set = sql query 



	}catch(PDOException $ex){



		$conn = null; // close connection 



		return "Bad sql";



	}







	$rows = array(); 







	while($row = $proc_get_stores->fetch(PDO::FETCH_ASSOC)){



		$rows[] = $row; // add row to array



	}







	$retVal = json_encode($rows); 



	$conn = null; // close connection 







	return $retVal; 



}







function get_all_sales()



{



	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}











	$sql = "SELECT * FROM sales"; //'CALL get_all_sales()'; // stored procedure 







	$proc_get_sales = $conn->prepare($sql);



	



	try{







		$rs = $proc_get_sales->execute(); // result set = sql query 



	}catch(PDOException $ex){



		$conn = null; // close connection 



		return "Bad sql";



	}







	$rows = array(); 







	while($row = $proc_get_sales->fetch(PDO::FETCH_ASSOC)){



		$rows[] = $row; // add row to array



	}







	$retVal = json_encode($rows); 



	$conn = null; // close connection 







	return $retVal; 



}



	



*/	



function get_sales_by_title($title_id)



{







	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}







	$conn->exec("SET CHARACTER SET utf8"); 







	$sql = "SELECT * FROM sales JOIN titles ON sales.title_id=titles.title_id JOIN stores ON stores.stor_id=sales.stor_id WHERE titles.title_id = ?"; 







	$stmt = $conn->prepare($sql);







       	try{







		$stmt->execute(array($title_id)); 







		// exception not thrown - send as JSON



		



		$rows = array(); 







		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){



			$rows[] = $row; // add each row to array



		}







		$retVal = json_encode($rows); 







	}catch(PDOException $e) {







		$retVal = "error getting books: " . $e->getMessage();



	}



	 



	// close connection 



	$stmt->closeCursor(); 



	$stmt = null; 



	$conn = null; // close connection







	return $retVal; 



		



	}// end function







function get_titles_by_id($title_id)



{







	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}







	$conn->exec("SET CHARACTER SET utf8"); 







	$sql = "SELECT * FROM titles WHERE title_id = ?"; 







	$stmt = $conn->prepare($sql);







       	try{







		$stmt->execute(array($title_id)); 







		// exception not thrown - send as JSON







		$rows = array(); 







		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){



			$rows[] = $row; // add each row to array



		}







		$retVal = json_encode($rows); 







	}catch(PDOException $e) {







		$retVal = "error getting titles: " . $e->getMessage();



	}



	 



	// close connection 



	$stmt->closeCursor(); 



	$stmt = null; 



	$conn = null; // close connection







	return $retVal; 



		



	}







function delete_title($title_id)



{



	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}







	$conn->exec("SET CHARACTER SET utf8"); 







	$sql = "DELETE FROM titles WHERE title_id = ?"; 



	



	$stmt = $conn->prepare($sql);







	try{







		$stmt->execute(array($title_id)); 







		// exception not thrown - send as JSON







		$rows = array(); 







		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){



			$rows[] = $row; // add each row to array



		}



	



	



	$retVal = json_encode($rows); 







	}catch(PDOException $e){







    		$retVal = "error deleting titles: " . $e->getMessage();



    	}







	// close connection 



	$stmt->closeCursor(); 



	$stmt = null; 



	$conn = null; // close connection







	return $retVal; 







}







function add_title($title_id)



{



	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}







	$conn->exec("SET CHARACTER SET utf8"); 







	$sql = "INSERT INTO titles (title, type, price, sales, notes) VALUES (?,?,?,?,?)";



	



	$stmt = $conn->prepare($sql);







	try{







		$stmt->execute(array($title_id)); 







		// exception not thrown - send as JSON







		$rows = array(); 







		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){



			$rows[] = $row; // add each row to array



		}



	



	



	$retVal = json_encode($rows); 







	}catch(PDOException $e){







    		$retVal = "error deleting titles: " . $e->getMessage();



    	}







	// close connection 



	$stmt->closeCursor(); 



	$stmt = null; 



	$conn = null; // close connection







	return $retVal; 



}







function update_title($title_id)



{



	$conn; 







	try{







		$conn = connect_to_pubs(); 







	}catch(PDOException $ex){



		return "open error: " . mysqli_connect_error() ; 



	}







	$conn->exec("SET CHARACTER SET utf8"); 







	$sql = "UPDATE titles SET ?=? WHERE title_id=?";



	



	$stmt = $conn->prepare($sql);







	try{







		$stmt->execute(array($title_id)); 







		// exception not thrown - send as JSON







		$rows = array(); 







		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){



			$rows[] = $row; // add each row to array



		}



	



	



	$retVal = json_encode($rows); 







	}catch(PDOException $e){







    		$retVal = "error deleting titles: " . $e->getMessage();



    	}







	// close connection 



	$stmt->closeCursor(); 



	$stmt = null; 



	$conn = null; // close connection







	return $retVal; 



}



?> 