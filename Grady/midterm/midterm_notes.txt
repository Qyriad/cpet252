[root@nhti3-64 ~]# ls
anaconda-ks.cfg  Documents  Music     Public  Templates  Videos
Desktop          Downloads  Pictures  SQL     vboxcd     workspace
[root@nhti3-64 ~]# cd Downloads
[root@nhti3-64 Downloads]# ls
sales_process_req.php  sales_pubs_dal.php
[root@nhti3-64 Downloads]# cd 
[root@nhti3-64 ~]# cd /var/www/html
[root@nhti3-64 html]# ls
deleteAuthor.php  js               redirect_00.php  storedProc_01.php
exam              pdo_test_1.php   redirect_01.php  storedProc_02.php
images            php_foreach.php  redirect_02.php  verify.php
info.php          prj              rest
[root@nhti3-64 html]# mkdir sales
[root@nhti3-64 html]# ls
deleteAuthor.php  js               redirect_00.php  sales
exam              pdo_test_1.php   redirect_01.php  storedProc_01.php
images            php_foreach.php  redirect_02.php  storedProc_02.php
info.php          prj              rest             verify.php
[root@nhti3-64 html]# cd
[root@nhti3-64 ~]# cd Downloads
[root@nhti3-64 Downloads]# cp *.*  /var/www/html/sales
[root@nhti3-64 ~]# cd
[root@nhti3-64 ~]# cd /var/www/html/sales
[root@nhti3-64 sales]# ls
sales_process_req.php  sales_pubs_dal.php

MariaDB [pubs]> describe sales;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| stor_id  | char(4)     | NO   | PRI | NULL    |       |
| ord_num  | varchar(20) | NO   | PRI | NULL    |       |
| ord_date | char(8)     | NO   |     | NULL    |       |
| qty      | smallint(6) | NO   |     | NULL    |       |
| payterms | varchar(12) | NO   |     | NULL    |       |
| title_id | varchar(6)  | NO   | PRI | NULL    |       |
+----------+-------------+------+-----+---------+-------+

MariaDB [pubs]> describe stores;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| stor_id      | char(4)     | NO   | PRI | NULL    |       |
| stor_name    | varchar(40) | YES  |     | NULL    |       |
| stor_address | varchar(40) | YES  |     | NULL    |       |
| city         | varchar(20) | YES  |     | NULL    |       |
| state        | char(2)     | YES  |     | NULL    |       |
| zip          | char(5)     | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+

MariaDB [pubs]> describe titles;
+-----------+---------------+------+-----+-----------+-------+
| Field     | Type          | Null | Key | Default   | Extra |
+-----------+---------------+------+-----+-----------+-------+
| title_id  | varchar(6)    | NO   | PRI | NULL      |       |
| title     | varchar(80)   | NO   |     | NULL      |       |
| type      | char(12)      | NO   |     | UNDECIDED |       |
| pub_id    | char(4)       | YES  |     | NULL      |       |
| price     | decimal(4,2)  | YES  |     | NULL      |       |
| advance   | decimal(10,2) | YES  |     | NULL      |       |
| royalty   | int(11)       | YES  |     | NULL      |       |
| ytd_sales | int(11)       | YES  |     | NULL      |       |
| notes     | varchar(200)  | YES  |     | NULL      |       |
+-----------+---------------+------+-----+-----------+-------+
