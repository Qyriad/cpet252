USE pubs;

DROP PROCEDURE IF EXISTS get_all_books;
DROP PROCEDURE IF EXISTS add_book;
DROP PROCEDURE IF EXISTS get_book;
DROP PROCEDURE IF EXISTS update_book;
DROP PROCEDURE IF EXISTS delete_book;
DROP PROCEDURE IF EXISTS get_all_stores;
DROP PROCEDURE IF EXISTS add_store;
DROP PROCEDURE IF EXISTS get_store;
DROP PROCEDURE IF EXISTS update_store;
DROP PROCEDURE IF EXISTS delete_store;
DROP PROCEDURE IF EXISTS get_all_sales;
DROP PROCEDURE IF EXISTS add_sale;
DROP PROCEDURE IF EXISTS get_sale;
DROP PROCEDURE IF EXISTS update_sale;
DROP PROCEDURE IF EXISTS delete_sale;

DELIMITER //

CREATE PROCEDURE get_all_books()
READS SQL DATA
BEGIN

SELECT * FROM titles;

END //

CREATE PROCEDURE add_book(IN _title_id varchar(6), _title varchar(80), _type char(12), _pub_id char(4), _price decimal(15,2), _advance decimal(15,2), _royalty int, _ytd_sales int, _notes varchar(200), _pubdate datetime)
READS SQL DATA
BEGIN

INSERT titles VALUES (_title_id, _title, _type, _pub_id, _price, _advance, _royalty, _ytd_sales, _notes, _pubdate); 

END //

CREATE PROCEDURE get_book(IN _title_id varchar(6))
READS SQL DATA
BEGIN

SELECT * FROM titles WHERE title_id = _title_id; 

END //

CREATE PROCEDURE update_book(IN _title_id varchar(6), _title varchar(80), _type char(12), _pub_id char(4), _price decimal(15,2), _advance decimal(15,2), _royalty int, _ytd_sales int, _notes varchar(200), _pubdate datetime)
READS SQL DATA
BEGIN

UPDATE titles SET ( title_id = _title_id, title = _title, type=_type, pub_id=_pub_id, price=_price, advance=_advance, royalty=_royalty, ytd_sales=_ytd_sales, notes=_notes, pubdate=_pubdate);

END //

CREATE PROCEDURE delete_book(IN _title_id varchar(6))
READS SQL DATA
BEGIN

DELETE FROM titles WHERE title_id = _title_id;

END //

CREATE PROCEDURE get_all_stores()
READS SQL DATA
BEGIN

SELECT * FROM stores;

END //

CREATE PROCEDURE add_store(IN _stor_id char(4), _stor_name varchar(40), _stor_address varchar(40), _city varchar(20), _state char(2), _zip char(5))
READS SQL DATA
BEGIN

INSERT stores VALUES (_stor_id, _stor_name, _stor_address, _city, _state, _zip); 

END //

CREATE PROCEDURE get_store(IN _stor_id char(4))
READS SQL DATA
BEGIN

SELECT * FROM stores WHERE stor_id = _stor_id; 

END //

CREATE PROCEDURE update_store(IN _stor_id char(4), _stor_name varchar(40), _stor_address varchar(40), _city varchar(20), _state char(2), _zip char(5))
READS SQL DATA
BEGIN

UPDATE stores SET ( stor_id = _stor_id, stor_name = _stor_name, stor_address=_stor_address, city=_city, state=_state, zip=_zip);

END //

CREATE PROCEDURE delete_store(IN _stor_id char(4))
READS SQL DATA
BEGIN

DELETE FROM stores WHERE stor_id = _stor_id;

END //

CREATE PROCEDURE get_all_sales()
READS SQL DATA
BEGIN

SELECT * FROM sales;

END //

CREATE PROCEDURE add_sale(IN _stor_id char(4), _ord_num varchar(20), _ord_date datetime, _qty smallint, _payterms varchar(12), _title_id varchar(6))
READS SQL DATA
BEGIN

INSERT sales VALUES (_stor_id, _ord_num, _ord_date, _qty, _payterms, _title_id); 

END //

CREATE PROCEDURE get_sale(IN _ord_num varchar(20))
READS SQL DATA
BEGIN

SELECT * FROM sales WHERE ord_num = _ord_num; 

END //

CREATE PROCEDURE update_sale(IN _stor_id char(4), _ord_num varchar(20), _ord_date datetime, _qty smallint, _payterms varchar(12), _title_id varchar(6))
READS SQL DATA
BEGIN

UPDATE sales SET ( stor_id = _stor_id, ord_num = _ord_num, ord_date=_ord_date, qty=_qty, payterms=_payterms, title_id=_title_id);

END //

CREATE PROCEDURE delete_sale(IN _ord_num varchar(20))
READS SQL DATA
BEGIN

DELETE FROM sales WHERE ord_num = _ord_num;

END //
DELIMITER ;
