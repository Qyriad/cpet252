<?php 
include 'pubs_dal.php'; 

$result = "UNDEFINED"; 
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{

	$req = $_POST['request']; 

	switch($req)
	{
	case "set_au_key":
		$_SESSION['last_au_key'] = $_POST['au_id']; 
		if (isset($_SESSION['last_au_key'])) {
		  $result = $_SESSION['last_au_key'];
		} else {
		  $result = "NO Record";
		}
		break; 

	case "get_last_au":
		if (isset($_SESSION['last_au_key'])) {
		  $result = get_author($_SESSION['last_au_key']);
		} else {
		  $result = "NO Record";
		}
		unset($_SESSION['last_au_key']);
		break;
		
	case "get_last_au_books":
		if (isset($_SESSION['last_au_key'])) {
		  $result = get_books_by_author($_SESSION['last_au_key']);
		} else {
		  $result = "NO Record";
		}
		unset($_SESSION['last_au_key']);
		break;
		
	case "get_authors":
		$result = get_all_authors(); 
		break; 
		
	case "get_books":
		$result = get_all_books(); 
		break; 

	case "get_au_books":
		$result = get_books_by_author($_POST['au_id']); 
		break; 

	case "delete_author":
		$result = delete_author($_POST['au_id']); 
		break; 
		
	case "add_author":
		$result = add_author($_POST['au_id'], $_POST['fname'], $_POST['lname'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['contract']); 
		break; 
		
	case "edit_author":
		$result = edit_author($_POST['au_id'], $_POST['fname'], $_POST['lname'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['contract']); 
		break; 
	
	case "add_author_book":
		$result = add_author_book($_POST['au_id'], $_POST['title_id']); 
		break; 
		
	case "add_book":
		$result = add_book($_POST['title_id'], $_POST['title'], $_POST['type'], $_POST['pub_id'], $_POST['price'], $_POST['advance'], $_POST['royalty'], $_POST['ytd_sales'], $_POST['notes'], $_POST['pubdate']); 
		break; 

	case "add_book_and_au_book":
		$result = add_book_and_au_book($_POST['au_id'], $_POST['title_id'], $_POST['title'], $_POST['type'], $_POST['pub_id'], $_POST['price'], $_POST['advance'], $_POST['royalty'], $_POST['ytd_sales'], $_POST['notes'], $_POST['pubdate']); 
		break; 
	default: 
		$result = "unknown request: " . $req; 

	}// end switch on request

}else if($_SERVER['REQUEST_METHOD'] == 'GET'){



}

echo $result; 

?>
