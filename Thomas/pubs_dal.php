<?php

function connect_to_pubs()
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=pubs'; 
	$user = 'root';
	$password = 'root'; 

	$handle = new PDO($dsn, $user, $password); 
	$handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

	return $handle; 
}

function get_author($au_id)
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}


	$sql = 'CALL get_au(?)'; // stored procedure 

	$stmt = $conn->prepare($sql);
	
	try{

		$rs = $stmt->execute(array($au_id));  // result set = sql query 
	}catch(PDOException $ex){
		$conn = null; // close connection 
		return "Bad sql";
	}
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$retVal = json_encode($row); 
	$conn = null; // close connection 

	return $retVal; 
}

function get_all_books()
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}


	$sql = 'CALL get_all_books()'; // stored procedure 

	$proc_get_authors = $conn->prepare($sql);
	
	try{

		$rs = $proc_get_authors->execute(); // result set = sql query 
	}catch(PDOException $ex){
		$conn = null; // close connection 
		return "Bad sql";
	}

	$rows = array(); 

	while($row = $proc_get_authors->fetch(PDO::FETCH_ASSOC)){
		$rows[] = $row; // add row to array
	}

	$retVal = json_encode($rows); 
	$conn = null; // close connection 

	return $retVal; 

}

function get_all_authors()
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}


	$sql = 'CALL get_all_authors()'; // stored procedure 

	$proc_get_authors = $conn->prepare($sql);
	
	try{

		$rs = $proc_get_authors->execute(); // result set = sql query 
	}catch(PDOException $ex){
		$conn = null; // close connection 
		return "Bad sql";
	}

	$rows = array(); 

	while($row = $proc_get_authors->fetch(PDO::FETCH_ASSOC)){
		$rows[] = $row; // add row to array
	}

	$retVal = json_encode($rows); 
	$conn = null; // close connection 

	return $retVal; 

}// end get_all_authors
	
function get_books_by_author($au_id)
{

	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "SELECT * FROM titles T JOIN titleauthor TA ON T.title_id = TA.title_id WHERE TA.au_id = ?"; 

	$stmt = $conn->prepare($sql);

       	try{
		$stmt->execute(array($au_id)); 
		$rows = array(); 

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$rows[] = $row; // add each row to array
		}

		$retVal = json_encode($rows); 

	}catch(PDOException $e) {

		$retVal = "error getting books: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal; 
		
	}// end function get author info
function delete_author($au_id)
{
	$conn; 

	try{
		$conn = connect_to_pubs(); 
	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "CALL delete_au(?)"; 
	$stmt = $conn->prepare($sql);

    try{
		$stmt->execute(array($au_id)); 
		$retVal = true; 
	}catch(PDOException $e) {

		$retVal = "error deleting author: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal;
}

function add_author($au_id, $fname, $lname, $phone, $address, $city, $state, $zip, $contract)
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "CALL add_au(?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
	$stmt = $conn->prepare($sql);

       	try{

		$stmt->execute(array($au_id, $lname, $fname, $phone, $address, $city, $state, $zip, $contract)); 

		$retVal = true; 

	}catch(PDOException $e) {

		$retVal = "error adding author: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal;
}

function edit_author($au_id, $fname, $lname, $phone, $address, $city, $state, $zip, $contract)
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "CALL edit_au(?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
	$stmt = $conn->prepare($sql);

       	try{

		$stmt->execute(array($au_id, $lname, $fname, $phone, $address, $city, $state, $zip, $contract)); 

		$retVal = true; 

	}catch(PDOException $e) {

		$retVal = "error editing author: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal;
}

function add_author_book($au_id, $title_id)
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "CALL add_au_book(?, ?)"; 
	$stmt = $conn->prepare($sql);

       	try{

		$stmt->execute(array($au_id, $title_id)); 

		$retVal = true; 

	}catch(PDOException $e) {

		$retVal = "error adding book to author: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal;
}

function add_book($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes, $pubdate)
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "CALL add_book(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
	$stmt = $conn->prepare($sql);

	$date = strtotime($pubdate);
	$date = date('Y-m-d H:i:s', $date);
       	try{

		$stmt->execute(array($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes, $date)); 

		$retVal = true; 

	}catch(PDOException $e) {

		$retVal = "error adding author: " . $e->getMessage();
	}
	 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal;
}

function add_book_and_au_book($au_id, $title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes, $pubdate)
{
	$conn; 

	try{

		$conn = connect_to_pubs(); 

	}catch(PDOException $ex){
		return "open error: " . mysqli_connect_error() ; 
	}

	$conn->exec("SET CHARACTER SET utf8"); 

	$sql = "CALL add_book(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
	$stmt = $conn->prepare($sql);

	$date = strtotime($pubdate);
	$date = date('Y-m-d H:i:s', $date);
       	try{

		$stmt->execute(array($title_id, $title, $type, $pub_id, $price, $advance, $royalty, $ytd_sales, $notes, $date)); 

		$retVal = 1; 

	}catch(PDOException $e) {

		$retVal = "error adding author: " . $e->getMessage();
	}
	
	$sql = "CALL add_au_book(?, ?)"; 
	$stmt = $conn->prepare($sql);

       	try{

		$stmt->execute(array($au_id, $title_id)); 

		if($retVal == 1) {$retval=true}

	}catch(PDOException $e) {

		$retVal = "error adding author: " . $e->getMessage();
	}
	 
	// close connection 
	$stmt->closeCursor(); 
	$stmt = null; 
	$conn = null; // close connection

	return $retVal;
}
?> 