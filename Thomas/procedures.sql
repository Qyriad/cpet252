USE pubs;

DROP PROCEDURE IF EXISTS add_au;
DROP PROCEDURE IF EXISTS delete_au;
DROP PROCEDURE IF EXISTS edit_au;
DROP PROCEDURE IF EXISTS add_au_book;
DROP PROCEDURE IF EXISTS get_au;
DROP PROCEDURE IF EXISTS add_book;

DELIMITER //

CREATE PROCEDURE add_au(IN _au_id varchar(11), _lname varchar(40), _fname varchar(20), _phone char(12), _address varchar(40), _city varchar(20), _state char(2), _zip char(5), _contract bit)
READS SQL DATA
BEGIN

INSERT authors VALUES (_au_id, _lname, _fname, _phone, _address, _city, _state, _zip, _contract); 

END //

CREATE PROCEDURE delete_au(IN _au_id varchar(11))
READS SQL DATA
BEGIN

DELETE FROM authors WHERE au_id = _au_id;

END //

CREATE PROCEDURE edit_au(IN _au_id varchar(11), lname varchar(40), fname varchar(20), _phone char(12), _address varchar(40), _city varchar(20), _state char(2), _zip char(5), _contract bit)
READS SQL DATA
BEGIN

UPDATE authors SET au_lname=lname, au_fname=fname, phone=_phone, address=_address, city=_city, state=_state, zip=_zip, contract=_contract WHERE au_id LIKE _au_id; 

END //

CREATE PROCEDURE add_au_book(IN _au_id varchar(11), _title_id varchar(6))
READS SQL DATA
BEGIN

INSERT INTO titleauthor (au_id, title_id) VALUES (_au_id, _title_id);

END //

CREATE PROCEDURE get_au(IN _au_id varchar(11))
READS SQL DATA
BEGIN

SELECT * FROM authors WHERE au_id = _au_id;

END //

CREATE PROCEDURE add_book(IN _title_id varchar(6), _title varchar(80), _type char(12), _pub_id char(4), _price decimal(15,2), _advance decimal(15,2), _royalty int, _ytd_sales int, _notes varchar(200), _pubdate datetime)
READS SQL DATA
BEGIN

INSERT titles VALUES (_title_id, _title, _type, _pub_id, _price, _advance, _royalty, _ytd_sales, _notes, _pubdate); 

END //

CREATE PROCEDURE get_all_books()
READS SQL DATA
BEGIN

SELECT * FROM titles;

END //
DELIMITER ;
