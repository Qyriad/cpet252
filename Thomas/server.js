//this allows the requested port to be opened
//iptables -I INPUT -p tcp --dport 8081 -j ACCEPT

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');
const db = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'root',
	database:'pubs'
});
db.connect();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Book Router
var bookRouter = express.Router();
bookRouter.get('/', function(req, res) { 	//gets all books
	db.query('SELECT * FROM titles;', function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
bookRouter.get('/:id', function(req, res) { 	//gets a book with id  *not used*
	var sqlString = 'SELECT * FROM titles WHERE title_id = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
bookRouter.post('/', function(req, res) { 	//adds a book
	var sqlString = 'INSERT titles VALUES (\'' + req.body.title_id + '\', \'' + req.body.title.replace("'", "''") + '\', \'' + req.body.type.replace("'", "''") + '\', \'' + req.body.pub_id + '\', \'' + req.body.price + '\', \'' + req.body.advance + '\', \'' + req.body.royalty + '\', \'' + req.body.ytd_sales + '\', \'' + req.body.notes.replace("'", "''") + '\');'
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
bookRouter.put('/:id', function(req, res) { 	//updates a book
	var sqlString = 'UPDATE titles SET title=\'' + req.body.title.replace("'", "''") + '\', type=\'' + req.body.type.replace("'", "''") + '\', pub_id=\'' + req.body.pub_id + '\', price=\'' + req.body.price + '\', advance=\'' + req.body.advance + '\', royalty=\'' + req.body.royalty + '\', ytd_sales=\'' + req.body.ytd_sales + '\', notes=\'' + req.body.notes.replace("'", "''") + '\' WHERE title_id=\'' + req.params.id + '\';'
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
bookRouter.delete('/:id', function(req, res) { 	//deletes a book  *shouldn't be used*
	var sqlString = 'DELETE FROM titles WHERE title_id = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
app.use('/books', bookRouter);

//Store Router
var storeRouter = express.Router();
storeRouter.get('/', function(req, res) { 	//gets all stores
	db.query('SELECT * FROM stores;', function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
storeRouter.get('/:id', function(req, res) { 	//gets a store with id  *not used*
	var sqlString = 'SELECT * FROM stores WHERE stor_id = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
storeRouter.get('/state', function(req, res) { 	//gets all stores sorted by state  *not used*

	db.query('SELECT * FROM stores ORDER BY state;', function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
storeRouter.get('/state/:id', function(req, res) { //gets stores from a state  *not used*
	var sqlString = 'SELECT * FROM stores WHERE state = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
storeRouter.get('/zip', function(req, res) { 	//gets all stores sorted by zip code  *not used*
	db.query('SELECT * FROM stores ORDER BY zip;', function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
storeRouter.post('/', function(req, res) { 	//adds a store
	var sqlString = 'INSERT stores VALUES (\'' + req.body.stor_id + '\', \'' + req.body.stor_name.replace("'", "''") + '\', \'' + req.body.stor_address.replace("'", "''") + '\', \'' + req.body.city.replace("'", "''") + '\', \'' + req.body.state + '\', \'' + req.body.zip + '\');'
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
		res.end();
	});
});
storeRouter.put('/:id', function(req, res) { 	//updates a store
	var sqlString = 'UPDATE stores SET stor_name=\'' + req.body.stor_name.replace("'", "''") + '\', stor_address=\'' + req.body.stor_address.replace("'", "''") + '\', city=\'' + req.body.city.replace("'", "''") + '\', state=\'' + req.body.state + '\', zip=\'' + req.body.zip + '\' WHERE stor_id=\'' + req.params.id + '\';'
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
storeRouter.delete('/:id', function(req, res) { //deletes a store  *shouldn't be used*
	var sqlString = 'DELETE FROM stores WHERE stor_id = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
app.use('/stores', storeRouter);

//Sales Router
var salesRouter = express.Router();
salesRouter.get('/', function(req, res) { 	//gets all sales
	db.query('SELECT stores.stor_id, stores.stor_name, ord_num, ord_date, qty, payterms, titles.title, titles.title_id FROM sales INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;', function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/:id', function(req, res) { 	//gets all sales with id  *not used*
	var sqlString = 'SELECT stor_id, ord_num, ord_date, qty, payterms, title_id FROM sales WHERE ord_num = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/store', function(req, res) { 	//gets all sales sorted by store id  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales ORDER BY stor_id INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/store/book', function(req, res) { //gets all sales sorted by store id then by title id  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales ORDER BY stor_id, title_id INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/store/:id', function(req, res) { //gets all sales for a store with store id
	var sqlString = 'SELECT stores.stor_id, stores.stor_name, ord_num, ord_date, qty, payterms, titles.title, titles.title_id FROM sales INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id WHERE sales.stor_id = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/book', function(req, res) { 	//gets all sales sorted by book id  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales ORDER BY title_id INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/book/:id', function(req, res) { //gets all sales for a book with book id
	var sqlString = 'SELECT stores.stor_id, stores.stor_name, ord_num, ord_date, qty, payterms, titles.title, titles.title_id FROM sales INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id WHERE sales.title_id = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/qty', function(req, res) { 	//gets all sales sorted by quantity  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales ORDER BY qty DESC INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/payterms', function(req, res) { //gets all sales sorted by payterms  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales ORDER BY payterms INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/payterms/:id', function(req, res) { //gets sales with a certain payterm  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id WHERE payterms = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/date', function(req, res) { 	//gets all sales sorted by order date  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales ORDER BY ord_date INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id;';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.get('/date/:id', function(req, res) { //gets sales on a specific date  *not used*
	var sqlString = 'SELECT stores.stor_name, ord_num, ord_date, qty, payterms, titles.title FROM sales INNER JOIN stores ON sales.stor_id = stores.stor_id INNER JOIN titles ON titles.title_id = sales.title_id WHERE ord_date = \'' + req.params.id + '\'';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.post('/', function(req, res) { 	//adds a sale
	var sqlString = 'INSERT sales VALUES (\'' + req.body.stor_id + '\', \'' + req.body.ord_num + '\', \'' + req.body.ord_date + '\', \'' + req.body.qty + '\', \'' + req.body.payterms + '\', \'' + req.body.title_id + '\');'
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
		res.end();
	});
});
salesRouter.put('/:id', function(req, res) { 	//updates a sale
	var sqlString = 'UPDATE sales SET stor_id=\'' + req.body.stor_id + '\', ord_date=\'' + req.body.ord_date + '\', qty=\'' + req.body.qty + '\', payterms=\'' + req.body.payterms + '\', title_id=\'' + req.body.title_id + '\' WHERE ord_num=\'' + req.params.id + '\';'
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});
salesRouter.delete('/:id', function(req, res) { //deletes a sale
	var sqlString = 'DELETE FROM sales WHERE ord_num = \'' + req.params.id + '\';';
	db.query(sqlString, function (error, results, fields) {
		if (error) throw error;
		return res.send(results)
	});
});

app.use('/sales', salesRouter);

module.exports = app;

//sets what ip and port to listen on
app.listen(8081, '64.72.2.73', function () {
    console.log('Node app is running on port 8081');
});