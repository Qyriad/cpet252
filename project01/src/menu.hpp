#pragma once

#include <string>
#include <vector>
#include <functional>
#include "config.hpp"

namespace qyr
{
	template<typename T>
	void wrap_increment(T *in, int max)
	{
		max--;
		(*in)++;
		if((*in) > static_cast<T>(max))
			(*in) = 0;
	}

	template<typename T>
	void wrap_decrement(T *in, int max)
	{
		max--;
		(*in)--;
		if(static_cast<signed int>(*in) < 0)
			(*in) = max;
	}

	struct option
	{
		std::string text;
		std::function<void(void)> callback;
	};

	class menu
	{
		WINDOW *scr;
		std::vector<option> options;
		int selected_index;

		public:
			menu() = delete;
			menu(menu const &rhs) = delete;
			menu operator=(menu const &rhs) = delete;
			menu(menu const &&rhs) = delete;
			menu operator=(menu const &&rhs) = delete;
			~menu();

			menu(WINDOW *screen, std::vector<option> options) : scr(screen), options(options), selected_index(0) {}
			void draw_center(int ch);
	};
}
