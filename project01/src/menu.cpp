#include "menu.hpp"

namespace qyr
{
	menu::~menu()
	{

	}

	void menu::draw_center(int c)
	{
		int max = this->options.size();
		wclear(this->scr);
		if(c == 'j' || c == KEY_DOWN)
		{
			wrap_increment(&this->selected_index, max);
		}
		else if(c == 'k' || c == KEY_UP)
		{
			wrap_decrement(&this->selected_index, max);
		}
		else if(c == KEY_ENTER || c == '\n')
		{
			std::invoke(this->options[selected_index].callback);
			werase(this->scr);
		}

		int line_start = LINES / 2 - max / 2;
		for(int i = 0; i < max; i++)
		{
			if(i == this->selected_index) attron(A_STANDOUT);
			mvwprintw(this->scr, line_start + i, COLS / 2 - (this->options[i].text.length() / 2), this->options[i].text.c_str());
			if(i == this->selected_index) attroff(A_STANDOUT);
		}
	}

}
