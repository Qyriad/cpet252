#include "text_input.hpp"

#include <climits>
#include <vector>
#include <algorithm>

namespace qyr
{
	std::string text_input::get(int max_chars)
	{
		printw("%s", this->prompt.c_str());
		start_row = getcury(this->scr);
		start_col = getcurx(this->scr);
		this->cur_col = this->start_col;
		this->cur_row = this->start_row;

		std::string outstr; 
		int c = getch();
		while(c != '\n')
		{
			if(c == KEY_BACKSPACE || c == KEY_DC || c == '\b' || c == 127 || c == 8)
			{
				if(cur_col <= start_col)
				{
					flash();
					beep();
				}
				else
				{
					cur_col--;
					move(cur_row, cur_col);
					delch();
					outstr.pop_back();
				}
			}
			else
			{
				if(cur_col - start_col < max_chars || max_chars == 0)
				{

					mvprintw(cur_row, cur_col, "%c", c);
					outstr += c;
					cur_col++;
				}
				else
				{
					flash();
					beep();
				}
			}
			refresh();
			c = getch();
		}
		return outstr;
	}

	std::string ip_addr_input::get(int max_chars)
	{
		printw("%s", this->prompt.c_str());
		start_row = getcury(this->scr);
		start_col = getcurx(this->scr);
		this->cur_col = this->start_col;
		this->cur_row = this->start_row;
		std::string outstr;

		std::vector<unsigned int> boundaries; // which columns are a . boundary for the IP
		std::vector<uint8_t> section_lengths;
		uint8_t sec_count = 0;

		int c = getch();
		while(c != '\n')
		{
			if(!std::isdigit(c) && !std::iscntrl(c) && c != '.' && c != '\n')
			{
				c = getch();
				continue;
			}
			
			move(cur_row, cur_col);
			if(c == KEY_BACKSPACE || c == KEY_DC || c == '\b' || c == 127 || c == 8)
			{
				if(cur_col <= start_col)
				{
					flash();
					beep();
				}
				else 
				{
					auto loc = std::find(boundaries.begin(), boundaries.end(), cur_col - 1);
					if(loc != boundaries.end()) // cur_col was found, so this is a . boundary
					{
						boundaries.erase(loc);
						// so backspace twice
						cur_col--;
						move(cur_row, cur_col);
						delch();
						outstr.pop_back();

						cur_col--;
						move(cur_row, cur_col);
						delch();
						outstr.pop_back();
						sec_count = section_lengths.back() - 1;
						section_lengths.pop_back();
					}
					else // not a . boundary, act normal
					{
						cur_col--;
						move(cur_row, cur_col);
						delch();
						outstr.pop_back();
						sec_count--;
					}
				}
			}
			else
			{
				if(cur_col - start_col < max_chars || max_chars == 0)
				{
					if(c == '.')
					{
						section_lengths.push_back(sec_count);
						boundaries.push_back(cur_col);
						sec_count = 0;
						addch('.');
						outstr += '.';
						cur_col++;
					}
					else if(sec_count == 3)
					{
						section_lengths.push_back(sec_count);
						boundaries.push_back(cur_col++);
						addch('.');
						outstr += '.';
						addch(c);
						outstr += c;
						cur_col++;
						sec_count = 1;
					}
					else
					{
						addch(c);
						outstr += c;
						cur_col++;
						sec_count++;
					}
				}
				else
				{
					flash();
					beep();
				}
			}
			refresh();
			c = getch();
		}

		return outstr;
	}
}
