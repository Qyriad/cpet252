#include <iostream>
#include <cstdio>
#include <cwchar>
#include <clocale>
#include <exception>
#include <sstream>

#include <execinfo.h>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

#define JSON_NOEXCEPTION
#include <nlohmann/json.hpp>

#include "config.hpp"
#include "menu.hpp"
#include "text_input.hpp"
#include "books_list.hpp"

namespace curl = curlpp;
namespace curlopts = curl::options;

using nlohmann::json;

// 魔法の魔女qyriad

void connect_to_host(std::string host);
void display_books(qyr::books_list &&bl);
void order_book(qyr::books_list const &bl, int selected_book_i);

int main(int, char **)
{
	try
	{
		setlocale(LC_ALL, "");
		initscr();
		start_color();
		cbreak(); // Don't buffer characters except escape characters like <C-c>
		noecho();
		keypad(stdscr, TRUE);
		erase();

		auto connect_to_hostname = []() -> void
		{
			move(LINES / 2 - 2, COLS / 2 - 15); // 15 is a fairly arbitrary choice
			qyr::text_input hostin(stdscr, "Hostname: ");
			std::string hostname = hostin.get();
			connect_to_host(hostname);
		};

		auto connect_to_ip_addr = []() -> void
		{
			move(LINES / 2 - 2, COLS / 2 - 5); // 5 is max IP addr len (7) / 2 rounded up
			qyr::ip_addr_input ipin(stdscr, "IP: ");
			std::string ip = ipin.get();
			connect_to_host(ip);
		};

		int c = 0;

		std::vector<qyr::option> opts = {{"Connect to Hostname", connect_to_hostname}, {"Connect to IP", connect_to_ip_addr}};

		qyr::menu m(stdscr, opts); // main menu loop

		while(c != 'q')
		{
			erase();
			m.draw_center(c);
			wrefresh(stdscr);
			c = getch();
		}
		refresh();
		//getch();

		endwin();
	}
	catch(curl::RuntimeError &e)
	{
		endwin();
		std::cout << e.what() << std::endl;
	}
	catch(curl::LogicError &e)
	{
		endwin();
		std::cout << "Exception at " << __FILE__ << ":" << __LINE__ << ":\n" << e.what() << std::endl;
		getchar();
	}
	catch(std::exception e)
	{
		endwin();
		std::cout << "Exception at " << __FILE__ << ":" << __LINE__ << ":\n" << e.what() << std::endl;
		getchar();
	}
	//printw("Hello world! 感じ");
	return 0;
}

void connect_to_host(std::string host)
{
	curl::Cleanup cleanup; // RAII init
	curl::Easy req;
	req.setOpt<curlopts::Url>(host + "/books");
	std::stringstream ss;
	curlopts::WriteStream ws(&ss);
	req.setOpt(ws);
	req.perform();

	json j;

	try
	{
		j = json::parse(ss.str());
	}
	catch(std::exception e)
	{
		endwin();
		std::cerr << "Error parsing json" << std::endl;
		getchar();
		abort();
	}
	display_books(qyr::books_list(j, host));
}

void display_books(qyr::books_list &&bl)
{
	using qyr::wrap_increment;
	using qyr::wrap_decrement;

	int c = 0;
	init_pair(1, COLOR_BLACK, COLOR_BLUE);
	unsigned int max = bl.books.size();
	unsigned int selected_i = 0;
	while(c != 'q')
	{
		struct
		{
			unsigned int begin;
			unsigned int title; // title end
			unsigned int type; // author end
			unsigned int price; // you get the idea
			unsigned int advance;
			unsigned int royalty;
			unsigned int ytd_sales;
		} boundaries;
		boundaries.begin = 1;
		boundaries.title = (COLS / 4);
		boundaries.type = boundaries.title + (COLS / 12);
		boundaries.price = boundaries.type + 12;
		boundaries.advance = boundaries.price + 12;
		boundaries.royalty = boundaries.advance + 12;
		boundaries.ytd_sales = boundaries.royalty + 14;

		switch(c)
		{
			case 'j':
				wrap_increment(&selected_i, max);
				break;
			case 'k':
				wrap_decrement(&selected_i, max);
				break;
			case 'a':
				order_book(bl, selected_i);
				erase();
				break;
		}

		erase();
		for(unsigned int i = 0; i < static_cast<unsigned int>(COLS); i++) { mvprintw(0, i, "-"); } // Fill—at the top
		// print headers
		mvprintw(1, boundaries.begin, " | Title");
		mvprintw(1, boundaries.title, "| Type");
		mvprintw(1, boundaries.type, "| Price");
		mvprintw(1, boundaries.price, "| Advance");
		mvprintw(1, boundaries.advance, "| Royalty");
		mvprintw(1, boundaries.royalty, "| YTD Sales");
		mvprintw(1, boundaries.ytd_sales, "| Notes");
		for(unsigned int i = 0; i < static_cast<unsigned int>(COLS); i++) { mvprintw(2, i, "-"); } // Fill—just below it


		unsigned int i = 0; // tracks line instead of book
		for(unsigned int book_i = 0; book_i < bl.books.size(); book_i++)
		{
			move(i + 3, 2);
			printw("| ");
			if(i / 2 == selected_i) // highlight selected
				attron(A_STANDOUT | COLOR_PAIR(1));

			// print title
			printw("%s", bl.books.at(book_i).title.c_str());
			// print type
			mvprintw(getcury(stdscr), boundaries.title, "| %s", bl.books.at(book_i).type.c_str());
			// print price
			mvprintw(getcury(stdscr), boundaries.type, "| $%02.2f", bl.books.at(book_i).price);
			// print advance
			mvprintw(getcury(stdscr), boundaries.price, "| $%02.2f", bl.books.at(book_i).advance);
			// print royalty
			mvprintw(getcury(stdscr), boundaries.advance, "| $%02.2f", bl.books.at(book_i).royalty);
			// print ytd sales
			mvprintw(getcury(stdscr), boundaries.royalty, "| $%02.2f", bl.books.at(book_i).ytd_sales);
			// print notes
			mvprintw(getcury(stdscr), boundaries.ytd_sales, "| %s", bl.books.at(book_i).notes.c_str());

			if(i / 2 == selected_i) // end highlight
				attroff(A_STANDOUT | COLOR_PAIR(1));

			mvprintw(40, 2, "ID: %s", bl.books.at(selected_i).identifier.c_str());

			// lower border
			for(int j = 0; j < COLS; j++)
			{
				mvprintw(i + 2, j, "-");
			}
			i += 2;
		}
		for(int j = 0; j < COLS; j++) mvprintw(i + 2, j, "-"); // I'm honestly not sure why I have to do this

		// Print controls on the bottom
		int controls_start = LINES - 3;
		mvprintw(controls_start, 2, "CONTROLS:\tAdd to order\t\t");
		mvprintw(controls_start + 1, 13, "\ta           \t\t");

		c = getch();
	}
	refresh();
}

void order_book(qyr::books_list const &bl, int selected_book_i)
{
	erase();
	auto book = bl.books.at(selected_book_i);
	// GET stores
	curl::Easy req;
	req.setOpt<curlopts::Url>(bl.host + "/stores");
	std::stringstream ss;
	curlopts::WriteStream ws(&ss);
	req.setOpt(ws);
	req.perform();

	json j;
	try
	{
		j = json::parse(ss.str());
	}
	catch(nlohmann::json::exception e)
	{
		endwin();
		std::cout << "Exception at " << __FILE__ << ":" << __LINE__ << ":\n" << e.what() << "; ID: " << e.id << std::endl;
		getchar();
	}
	qyr::stores_list sl(j, bl.host);

	struct
	{
		struct
		{
			int line;
			int col;
		} title_start;
		struct
		{
			int box_line;
			int name;
			int address;
		} stores;
	} locs;
	locs.title_start = { LINES / 19, COLS / 19};
	locs.stores.box_line = locs.title_start.line + 3;
	locs.stores.name = COLS / 4;
	locs.stores.address = locs.stores.name + (COLS / 4);
	mvprintw(locs.title_start.line, locs.title_start.col, "Title: ");
	attron(A_UNDERLINE);
	printw("%s", book.title.c_str());
	attroff(A_UNDERLINE);
	mvprintw(locs.title_start.line + 1, locs.title_start.col, "Price: $%02.2f\t\tAdvance: $%02.2f\t\tRoyalty: $%02.2f", book.price, book.advance, book.royalty);
	// print stores
	move(locs.stores.box_line, 1);
	for(int i = 0; i < COLS; i++) mvprintw(locs.stores.box_line, i, "-");
	int i = locs.title_start.line + 1; // tracks line instead of store
	for(int store_i = 0; store_i < sl.stores.size(); store_i++)
	{
		move(i + 3, 2);
		printw("| ");
		printw("%s", sl.stores.at(store_i).name.c_str());
		mvprintw(getcury(stdscr), locs.stores.name, "| %s %s, %s", sl.stores.at(store_i).address.c_str(), sl.stores.at(store_i).city.c_str(), sl.stores.at(store_i).state.c_str());

		// lower border
		for(int j = 0; j < COLS; j++)
		{
			mvprintw(i + 2, j, "-");
		}

		i += 2;
	}
	for(int j = 0; j < COLS; j++) mvprintw(i + 2, j, "-"); // I'm honestly not sure why I have to do this
	refresh();
	getch();
}
