#pragma once

#include <string>
#include <vector>
#include <nlohmann/json.hpp>

#include "config.hpp"

namespace qyr
{
	struct book
	{
		std::string identifier;
		std::string title;
		std::string type;
		std::string publisher_identifier;
		double price;
		double advance;
		double royalty;
		int ytd_sales;
		std::string notes;
	};

	void from_json(nlohmann::json const &j, book &b);

	class books_list
	{
		public:
			books_list() = delete;
			books_list(books_list const &rhs) = delete;
			books_list operator=(books_list const &rhs) = delete;
			books_list(books_list const &&rhs) = delete;
			books_list operator=(books_list const &&rhs) = delete;
			//~books_list();

			books_list(nlohmann::json json, std::string remote_host);

			std::vector<book> books;
			std::string host;
	};

	struct store
	{
		std::string identifier;
		std::string name;
		std::string address;
		std::string city;
		std::string state;
		std::string zip;
	};

	class stores_list
	{
		public:
			stores_list() = delete;
			stores_list(stores_list const &rhs) = delete;
			stores_list operator=(stores_list const &rhs) = delete;
			stores_list(stores_list const &&rhs) = delete;
			stores_list operator=(stores_list const &&rhs) = delete;

			stores_list(nlohmann::json json, std::string remote_host);

			std::vector<store> stores;
			std::string host;
	};
}
