#pragma once

#include <string>
#include "config.hpp"


namespace qyr
{
	class text_input
	{
		protected:
			WINDOW *scr;
			int start_col;
			int start_row;
			int cur_col;
			int cur_row;
			std::string input;
			std::string prompt;

		public:
			text_input() = delete;
			text_input(text_input const &rhs) = delete;
			text_input operator=(text_input const &rhs) = delete;
			text_input(text_input const &&rhs) = delete;
			text_input operator=(text_input const &&rhs) = delete;

			text_input(WINDOW *screen, char const *prompt) : scr(screen), prompt(prompt) {}
			virtual std::string get(int max_chars = 0);
	};

	class ip_addr_input : public text_input
	{
		public:
			ip_addr_input(WINDOW *screen, char const *prompt) : text_input(screen, prompt) {}
			std::string get(int max_chars = 15) override;
	};
}
