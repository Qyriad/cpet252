#include "books_list.hpp"

#include <string>
#include <exception>
#include <iostream>

namespace qyr
{
	books_list::books_list(nlohmann::json json, std::string remote_host)
	{
		books = std::vector<struct book>();
		for(auto &element : json)
		{
			struct book b = element;
			books.push_back(b);
		}
		host = remote_host;
	}

	void from_json(nlohmann::json const &j, book &b)
	{
		try
		{
			b.identifier = j.at("title_id").get<std::string>();
			b.title = j.at("title").get<std::string>();
			b.type = j.at("type").get<std::string>();
			b.publisher_identifier = j.at("pub_id").get<std::string>();
			if(j.at("price").is_string())
			{
				std::string price_str = j.at("price").get<std::string>();
				b.price = std::stod(price_str);
			}
			else b.price = j.at("price").get<double>();
			if(j.at("advance").is_string())
			{
				std::string advance_str = j.at("advance").get<std::string>();
				b.advance = std::stod(advance_str);
			}
			else b.advance = j.at("advance").get<double>();
			if(j.at("royalty").is_string())
			{
				std::string royalty_str = j.at("royalty").get<std::string>();
				b.royalty = std::stod(royalty_str);
			}
			else b.royalty = j.at("royalty").get<double>();
			if(j.at("ytd_sales").is_string())
			{
				std::string ytd_sales_str = j.at("ytd_sales").get<std::string>();
				b.ytd_sales = std::stoi(ytd_sales_str);
			}
			b.notes = j.at("notes").get<std::string>();
		}
		catch(nlohmann::json::exception e)
		{
			endwin();
			std::cout << "Exception at " << __FILE__ << ":" << __LINE__ << ":\n" << e.what() << "; ID: " << e.id << std::endl;
			getchar();
		}
	}

	stores_list::stores_list(nlohmann::json json, std::string remote_host)
	{
		stores = std::vector<struct store>();
		for(auto &element : json)
		{
			struct store s = element;
			stores.push_back(s);
		}
		host = remote_host;
	}

	void from_json(nlohmann::json const &j, store &s)
	{
		try
		{
			s.identifier = j.at("stor_id").get<std::string>();
			s.name = j.at("stor_name").get<std::string>();
			s.address = j.at("stor_address").get<std::string>();
			s.city = j.at("city").get<std::string>();
			s.state = j.at("state").get<std::string>();
			s.zip = j.at("zip").get<std::string>();
		}
		catch(nlohmann::json::exception e)
		{
			endwin();
			std::cout << "Exception at " << __FILE__ << ":" << __LINE__ << ":\n" << e.what() << "; ID: " << e.id <<  std::endl;
		}
	}
}
