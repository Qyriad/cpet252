#define CURSES_HAVE_CURSES_H
#define CURSES_HAVE_NCURSES_H
/* #undef CURSES_HAVE_NCURSES_NCURSES_H */
/* #undef CURSES_HAVE_NCURSES_CURSES_H */

#ifdef CURSES_HAVE_CURSES_H
#include <curses.h>
#elif defined(CURSES_HAVE_NCURSES_H)
#include <ncurses.h>
#elif defined(CURSES_HAVE_NCURSES_NCURSES_H)
#include <ncurses/ncurses.h>
#elif defined(CURSES_HAVE_NCURSES_CURSES_H)
#include <ncurses/curses.h>
#endif

